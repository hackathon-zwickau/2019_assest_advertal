package de.advertal.assest.worker;

import de.advertal.assest.models.database.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class EvaluationWorker {

    Thread thread;
    boolean stopRequested = false;

    public EvaluationWorker(){
        thread = new Thread(() -> {
            while(!stopRequested){
                TestRun run = TestRun.getTable().builder().where().eq("state", "SUBMITTED").query().first();
                if(run==null){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                try {
                    run.setState(ResultState.PENDING);
                    TestRun.getTable().update(run);
                    try {
                        TestResult testResult = new TestResult();
                        testResult.setState(ResultState.PENDING);
                        testResult.setTestRunId(run.getId());
                        List<SkillResult> skillResults = new ArrayList<>();
                        for(Skill skill : testResult.getSkills()){
                            SkillResult skillResult = new SkillResult();
                            skillResult.setSkillId(skill.getId());
                            skillResult.setTestRunId(run.getId());
                            skillResult.setState(ResultState.PENDING);
                            SkillResult.getTable().create(skillResult);
                            skillResult = SkillResult.getTable().builder().order("id", true).limit(1).query().first();
                            skillResults.add(skillResult);
                        }
                        TestResult.getTable().create(testResult);
                        testResult = TestResult.getTable().builder().order("id", true).limit(1).query().first();
                        for(SkillResult skillResult : skillResults){
                            skillResult.calculate();
                            skillResult.setState(ResultState.DONE);
                            SkillResult.getTable().update(skillResult);
                        }
                        testResult.calculate();
                        testResult.setState(ResultState.DONE);
                        TestResult.getTable().update(testResult);
                        run.setState(ResultState.DONE);
                        TestRun.getTable().update(run);
                    }catch (Exception ex){
                        ex.printStackTrace();
                        run.setState(ResultState.FAILED);
                        TestRun.getTable().update(run);
                    }
                }catch (Exception ex){

                }
            }
        });
    }

    public void start(){
        stopRequested = false;
        thread.start();
    }

    public void stop(){
        thread.interrupt();
    }

}
