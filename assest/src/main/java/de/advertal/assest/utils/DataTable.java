package de.advertal.assest.utils;

import java.util.ArrayList;
import java.util.List;

public class DataTable {

    List<List<String>> data = new ArrayList<>();

    public DataTable row(String... cols){
        data.add(new ArrayList<String>(){{
            for(String c : cols)
                add(c);
        }});
        return this;
    }

}
