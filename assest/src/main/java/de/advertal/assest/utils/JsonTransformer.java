package de.advertal.assest.utils;

import com.ftpix.sparknnotation.interfaces.BodyTransformer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import spark.ResponseTransformer;

public class JsonTransformer implements BodyTransformer, ResponseTransformer {
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public <T> T transform(String input, Class<T> clazz) {
        return gson.fromJson(input, clazz);
    }

    @Override
    public String render(Object obj) {
        return gson.toJson(obj);
    }
}
