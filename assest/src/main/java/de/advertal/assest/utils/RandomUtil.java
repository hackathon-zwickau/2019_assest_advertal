package de.advertal.assest.utils;

import de.advertal.assest.models.database.Item;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class RandomUtil {

    private static Map<String, Integer[]> sortCache = new HashMap<>();
    private static List<String> order = new ArrayList<>();

    public static void clearCache(){
        while(order.size()>10){
            String salt = order.get(0);
            order.remove(salt);
            sortCache.remove(salt);
        }
    }

    public static List<Item> randomizeItems(List<Item> items, String salt){
        Item[] out = new Item[items.size()];
        Integer[] order = RandomUtil.randomizedOrder(out.length, salt);
        for(int i=0; i< out.length; i++)
            out[i] = items.get(order[i]);
        return Arrays.asList(out);
    }

    public static Integer[] randomizedOrder(int size, String salt){
        if(sortCache.containsKey(salt))
            return sortCache.get(salt);
        int[] saltValues = salt(salt);
        Integer[] sort = randomizedOrder(size, saltValues[0], saltValues[1] , saltValues[2]);
        sortCache.put(salt, sort);
        order.add(salt);
        return sort;
    }

    private static int crossfoot(int value) {
        if (value <= 9) return value;
        return value%10 + crossfoot(value/10);
    }

    private static Integer[] randomizedOrder(int size, int shiftBefore, int jump, int shiftAfter){
        Integer[] data = new Integer[size];
        for(int i=0;i<data.length;i++)
            data[i] = i;
        data = shift(data, shiftBefore);
        int jumps = data.length;
        if(jumps>500)
            jumps = 500;
        jumps = jumps*jumps;
        for(int i=0;i<data.length*data.length;i++)
            data = jump(data, (i+i)%data.length, jump);
        data = shift(data, shiftAfter);
        return data;
    }

    private static Integer[] jump(Integer[] data, int position, int jump){
        int newPos = position + jump;
        if(newPos>=data.length)
            newPos = newPos%data.length;
        Integer[] out = new Integer[data.length];
        if(position == newPos){
            for(int i=0;i<data.length;i++)
                out[i] = data[i];
            return out;
        }
        if(newPos > position){
            for(int i=0;i<position;i++)
                out[i] = data[i];
            for(int i=newPos+1;i<data.length;i++)
                out[i] = data[i];
            for(int i=position+1;i<=newPos;i++)
                out[i-1] = data[i];
            out[newPos] = data[position];
            return out;
        }else{
            for(int i=position+1; i<data.length; i++)
                out[i] = data[i];
            for(int i=0; i<newPos;i++)
                out[i] = data[i];
            for(int i=newPos; i<position; i++)
                out[i+1] = data[i];
            out[newPos] = data[position];
            return out;
        }
    }

    private static Integer[] shift(Integer[] data, int positions){
        Integer[] out = new Integer[data.length];
        for(int i=0;i<data.length;i++){
            int newPos = i + positions;
            if(newPos>=data.length)
                newPos = newPos%data.length;
            out[newPos] = data[i];
        }
        return out;
    }

    private static int[] salt(String salt){
        String part1 = base64(md5(salt));
        String part2 = base64(md5(part1));
        String part3 = base64(md5(part2));
        return new int[]{
                shorten(part1),
                shorten(part2),
                shorten(part3)
        };
    }

    private static int shorten(String input){
        int value = 0;
        for(char c : input.toCharArray())
            value += c;
        while(value>100)
            value = crossfoot(value);
        return value;
    }

    private static String base64(String input){
        return Base64.getEncoder().encodeToString(input.getBytes());
    }

    private static String md5(String input){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hashInBytes = md.digest(input.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes)
                sb.append(String.format("%02x", b));
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return input;
    }

}
