package de.advertal.assest.utils;

public class PythonFormatChecker {

    public int totalLength;
    public int commentLength = 0;
    public int functionCount = 0;
    public int functionNameLengthTotal = 0;
    public int variableCount = 0;
    public int variableNameLengthTotal = 0;

    public PythonFormatChecker(String... lines){
        for(int i=0; i<lines.length; i++){
            totalLength += lines[i].length();
            char[] chars = lines[i].toCharArray();
            for(int c=0; c < chars.length; c++){
                if(chars[c] == '#'){
                    commentLength += chars.length - c;
                }
            }
            if(lines[i].startsWith("def ")){
                String functionName = lines[i].split("\\(")[0].substring(4);
                functionCount++;
                functionNameLengthTotal += functionName.length();
            }
            if(lines[i].contains("=") && !lines[i].endsWith(":")){
                String lhs = lines[i].split("=")[0];
                if(!lhs.contains("[")){
                    lhs = lhs.replace(" ", "");
                    variableCount++;
                    variableNameLengthTotal += lhs.length();
                }
            }
        }
        totalLength += lines.length - 1;
    }

}
