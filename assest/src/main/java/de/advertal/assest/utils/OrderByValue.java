package de.advertal.assest.utils;

import de.advertal.assest.models.database.Wording;
import de.advertal.assest.models.web.WebWording;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@NoArgsConstructor
@AllArgsConstructor
public class OrderByValue implements Comparator {

    boolean desc = false;

    public int compare(Object o, Object t1) {
        double v1 = 0;
        double v2 = 0;
        if(o instanceof Wording){
            v1 = ((Wording)o).getMinValue();
            v2 = ((Wording)t1).getMinValue();
        }
        if(o instanceof WebWording){
            v1 = ((WebWording)o).getMinValue();
            v2 = ((WebWording)t1).getMinValue();
        }
        if(v1 == v2)
            return 0;
        if(v1 < v2)
            return desc?1:-1;
        return desc?-1:1;
    }
}
