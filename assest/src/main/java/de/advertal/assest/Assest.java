package de.advertal.assest;

import com.ftpix.sparknnotation.Sparknotation;
import de.advertal.assest.utils.JsonTransformer;
import de.advertal.assest.models.database.*;
import de.advertal.assest.worker.EvaluationWorker;
import eu.bebendorf.ajorm.SQL;
import eu.bebendorf.ajorm.Table;
import freemarker.template.Configuration;
import lombok.Getter;
import spark.Spark;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

import java.io.IOException;

public class Assest {

    @Getter
    private static Assest instance;

    public static void main(String [] args){
        instance = new Assest();
        instance.start();
    }

    @Getter
    private TemplateEngine templateEngine;

    @Getter
    private Branding branding;
    @Getter
    private String cdn = "http://localhost/assest/";
    private EvaluationWorker evaluationWorker;

    public Assest(){
        branding = new Branding("Assest", "Assest", "https://advertal.de/impressum", "https://advertal.de/datenschutz/");
        templateEngine = generateFreemarkerEngine();
        evaluationWorker = new EvaluationWorker();
        SQL sql = MySQLConfig.get().getSQL();
        User.setTable(new Table<>(sql, User.class));
        User.getTable().migrate();
        Session.setTable(new Table<>(sql, Session.class));
        Session.getTable().migrate();
        Test.setTable(new Table<>(sql, Test.class));
        Test.getTable().migrate();
        TestSkill.setTable(new Table<>(sql, TestSkill.class));
        TestSkill.getTable().migrate();
        Item.setTable(new Table<>(sql, Item.class));
        Item.getTable().migrate();
        Skill.setTable(new Table<>(sql, Skill.class));
        Skill.getTable().migrate();
        Category.setTable(new Table<>(sql, Category.class));
        Category.getTable().migrate();
        TestRun.setTable(new Table<>(sql, TestRun.class));
        TestRun.getTable().migrate();
        SkillResult.setTable(new Table<>(sql, SkillResult.class));
        SkillResult.getTable().migrate();
        TestResult.setTable(new Table<>(sql, TestResult.class));
        TestResult.getTable().migrate();
        Answer.setTable(new Table<>(sql, Answer.class));
        Answer.getTable().migrate();
        Wording.setTable(new Table<>(sql, Wording.class));
        Wording.getTable().migrate();
    }

    public void start(){
        evaluationWorker.start();
        Spark.port(8080);
        Spark.staticFileLocation("public");
        Spark.exception(Exception.class, (e, request, response) -> {
            if(e instanceof NumberFormatException){
                NumberFormatException ex = (NumberFormatException)e;
                if(ex.getMessage().contains("\"images\""))
                    return;
            }
            e.printStackTrace();
        });
        Spark.notFound((request, response) -> {
            response.redirect("/");
            return "";
        });
        try {
            Sparknotation.init(new JsonTransformer());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static FreeMarkerEngine generateFreemarkerEngine(){
        FreeMarkerEngine engine = new FreeMarkerEngine();
        Configuration configuration = new Configuration();
        configuration.setClassLoaderForTemplateLoading(Assest.class.getClassLoader(),"/templates/");
        engine.setConfiguration(configuration);
        return engine;
    }

}
