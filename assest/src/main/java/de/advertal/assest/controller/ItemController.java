package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.*;
import de.advertal.assest.models.database.Item;
import de.advertal.assest.models.database.Skill;
import de.advertal.assest.models.database.User;
import de.advertal.assest.models.web.*;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;

import java.util.List;

@SparkController
public class ItemController extends Controller {

    @SparkGet(value = "/items")
    public String getItems(Request request){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        List<Skill> skills = Skill.getTable().queryForEq("customer_id", customerId);
        return VIEW(request, "items", ME("skills", skills));
    }

    @SparkPost(value = "/api/items", transformer = JsonTransformer.class)
    public StatusResponse createItem(Request request, @SparkBody ItemCreateRequest create){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        Item item = Item.getTable().builder().where().eq("skill_id", create.getSkill()).and().eq("question", create.getQuestion()).query().first();
        if(item!=null)
            return new StatusResponse(false, "Dieses Item existiert bereits");
        Skill skill = Skill.getTable().queryById(create.getSkill());
        if(skill==null || !skill.canRead(user))
            return new StatusResponse(false, "Der Skill wurde nicht gefunden");
        item = new Item();
        item.setSkillId(create.getSkill());
        item.setQuestion(create.getQuestion());
        item.setType(create.getType());
        item.setCustomerId(customerId);
        item.setInvert(create.isInvert());
        item.setAnswers(create.getAnswers());
        item.setCorrectAnswer(create.getCorrect());
        item.setMediaType(create.getMediaType());
        item.setMedia(create.getMedia());
        Item.getTable().create(item);
        return new StatusResponse(true, "Das Item wurde erstellt");
    }

    @SparkDelete(value = "/api/items/:item", transformer = JsonTransformer.class)
    public StatusResponse deleteItem(Request request, @SparkParam("item") int itemId){
        User user = USER(request);
        Item item = Item.getTable().queryById(itemId);
        if(item==null)
            return new StatusResponse(false, "Das Item wurde nicht gefunden");
        if(!user.getType().isAdmin() && item.getCustomerId() != user.getId())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung");
        item.delete();
        return new StatusResponse(true, "Das Item wurde gelöscht");
    }

    @SparkGet(value = "/api/items/:item", transformer = JsonTransformer.class)
    public EditItemResponse getEditItem(Request request, @SparkParam("item") int itemId){
        User user = USER(request);
        Item item = Item.getTable().queryById(itemId);
        if(item==null)
            return new EditItemResponse(false, "Das Item wurde nicht gefunden");
        if(!user.getType().isAdmin() && item.getCustomerId() != user.getId())
            return new EditItemResponse(false, "Dafür haben Sie keine Berechtigung");
        return new EditItemResponse(true, "Das Item wurde gefunden", item);
    }

    @SparkPost(value = "/api/items/:item", transformer = JsonTransformer.class)
    public StatusResponse updateItem(Request request, @SparkParam("item") int itemId, @SparkBody UpdateItemRequest update){
        User user = USER(request);
        Item item = Item.getTable().queryById(itemId);
        if(item==null)
            return new StatusResponse(false, "Das Item wurde nicht gefunden");
        if(!user.getType().isAdmin() && item.getCustomerId() != user.getId())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung");
        item.setQuestion(update.getQuestion());
        item.setInvert(update.isInvert());
        item.setType(update.getType());
        item.setMedia(update.getMedia());
        item.setMediaType(update.getMediaType());
        item.setAnswers(update.getAnswers());
        item.setCorrectAnswer(update.getCorrect());
        Item.getTable().update(item);
        return new StatusResponse(true, "Das Item wurde bearbeitet");
    }

}
