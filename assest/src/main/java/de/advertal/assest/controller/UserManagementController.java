package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.*;
import de.advertal.assest.models.database.Session;
import de.advertal.assest.models.database.User;
import de.advertal.assest.models.web.CreateCandidateRequest;
import de.advertal.assest.models.web.CreateUserRequest;
import de.advertal.assest.models.web.StatusResponse;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@SparkController
public class UserManagementController extends Controller {

    @SparkGet("/admin/customers")
    public String getCustomers(Request request){
        return VIEW(request, "admin/customers");
    }

    @SparkGet("/admin/admins")
    public String getAdmins(Request request){
        return VIEW(request, "admin/admins");
    }

    @SparkGet("/candidates")
    public String getCandidates(Request request){
        User user = USER(request);
        if(user.getType().isAdmin()){
            return VIEW(request, "candidates", ME("customers", User.getTable().queryForEq("type", "CUSTOMER")));
        }else{
            return VIEW(request, "candidates");
        }
    }

    @SparkPost(value = "/api/candidates", transformer = JsonTransformer.class)
    public StatusResponse createCandidate(Request request, @SparkBody CreateCandidateRequest create){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin()){
            if(create.getCustomer()==0){
                return new StatusResponse(false, "Es wurde kein Kunde ausgewählt.");
            }
            User customer = User.getTable().queryById(create.getCustomer());
            if(customer == null || !customer.getType().isCustomer()){
                return new StatusResponse(false, "Es wurde kein Kunde ausgewählt.");
            }
            customerId = customer.getId();
        }
        if(create.getFirstName().length()<2 || create.getLastName().length()<2){
            return new StatusResponse(false, "Ungültiger Name");
        }
        if(VALIDATE(create.getEmail(), Validator.EMAIL) != null){
            return new StatusResponse(false, "Ungültige E-Mail");
        }
        if(User.getTable().builder().where().eq("first_name", create.getFirstName()).and().eq("last_name", create.getLastName()).and().eq("type", "USER").and().eq("parent_id", customerId).query().first() != null){
            return new StatusResponse(false, "Dieser Bewerber existiert bereits");
        }
        User candidate = new User();
        candidate.setCreatedAt(Timestamp.from(Instant.now()));
        candidate.setType(User.Type.USER);
        candidate.setParentId(customerId);
        candidate.setFirstName(create.getFirstName());
        candidate.setLastName(create.getLastName());
        candidate.setGender(create.getGender());
        candidate.setEmail(create.getEmail());
        candidate.setPassword(UUID.randomUUID().toString());
        User.getTable().create(candidate);
        return new StatusResponse(true, "Der Bewerber wurde erstellt");
    }

    @SparkDelete(value = "/api/candidates/:candidate", transformer = JsonTransformer.class)
    public StatusResponse deleteCandidate(Request request, @SparkParam("candidate") int candidateId){
        User user = USER(request);
        User candidate = User.getTable().queryById(candidateId);
        if(candidate == null)
            return new StatusResponse(false, "Bewerber nicht gefunden!");
        if(!user.getType().isAdmin() && user.getId() != candidate.getParentId())
            return new StatusResponse(false, "Bewerber nicht gefunden!");
        candidate.deleteCandidate();
        return new StatusResponse(true, "Der Bewerber wurde gelöscht");
    }

    @SparkPost(value = "/api/customers", transformer = JsonTransformer.class)
    public StatusResponse createCustomer(Request request, @SparkBody CreateUserRequest create){
        User user = USER(request);
        if(!user.getType().isAdmin())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung!");
        if(create.getFirstName().length()<2 || create.getLastName().length()<2){
            return new StatusResponse(false, "Ungültiger Name");
        }
        if(VALIDATE(create.getEmail(), Validator.EMAIL) != null){
            return new StatusResponse(false, "Ungültige E-Mail");
        }
        if(User.getTable().builder().where().eq("first_name", create.getFirstName()).and().eq("last_name", create.getLastName()).and().eq("type", "CUSTOMER").query().first() != null){
            return new StatusResponse(false, "Dieser Benutzer existiert bereits");
        }
        if(create.getPassword().length() < 8 || create.getPassword().length() > 20)
            return new StatusResponse(false, "Ungültiges Passwort");
        User customer = new User();
        customer.setCreatedAt(Timestamp.from(Instant.now()));
        customer.setType(User.Type.CUSTOMER);
        customer.setFirstName(create.getFirstName());
        customer.setLastName(create.getLastName());
        customer.setGender(create.getGender());
        customer.setEmail(create.getEmail());
        customer.setPassword(create.getPassword());
        User.getTable().create(customer);
        return new StatusResponse(true, "Der Benutzer wurde erstellt");
    }

    @SparkDelete(value = "/api/customers/:customer", transformer = JsonTransformer.class)
    public StatusResponse deleteCustomer(Request request, @SparkParam("customer") int customerId){
        User user = USER(request);
        User customer = User.getTable().queryById(customerId);
        if(customer == null)
            return new StatusResponse(false, "Benutzer nicht gefunden!");
        if(!user.getType().isAdmin())
            return new StatusResponse(false, "Benutzer nicht gefunden!");
        for(Session session : Session.getTable().queryForEq("user_id", customer.getId())){
            if(session.isSU()) {
                session.returnSU();
            }else{
                Session.getTable().delete(session);
            }
        }
        customer.deleteCustomer();
        return new StatusResponse(true, "Der Benutzer wurde gelöscht");
    }

    @SparkPost(value = "/api/admins", transformer = JsonTransformer.class)
    public StatusResponse createAdmin(Request request, @SparkBody CreateUserRequest create){
        User user = USER(request);
        if(!user.getType().isAdmin())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung!");
        if(create.getFirstName().length()<2 || create.getLastName().length()<2){
            return new StatusResponse(false, "Ungültiger Name");
        }
        if(VALIDATE(create.getEmail(), Validator.EMAIL) != null){
            return new StatusResponse(false, "Ungültige E-Mail");
        }
        if(User.getTable().builder().where().eq("first_name", create.getFirstName()).and().eq("last_name", create.getLastName()).and().eq("type", "ADMIN").query().first() != null){
            return new StatusResponse(false, "Dieser Benutzer existiert bereits");
        }
        if(create.getPassword().length() < 8 || create.getPassword().length() > 20)
            return new StatusResponse(false, "Ungültiges Passwort");
        User admin = new User();
        admin.setCreatedAt(Timestamp.from(Instant.now()));
        admin.setType(User.Type.ADMIN);
        admin.setFirstName(create.getFirstName());
        admin.setLastName(create.getLastName());
        admin.setGender(create.getGender());
        admin.setEmail(create.getEmail());
        admin.setPassword(create.getPassword());
        User.getTable().create(admin);
        return new StatusResponse(true, "Der Benutzer wurde erstellt");
    }

    @SparkDelete(value = "/api/admins/:admin", transformer = JsonTransformer.class)
    public StatusResponse deleteAdmin(Request request, @SparkParam("admin") int adminId){
        User user = USER(request);
        User admin = User.getTable().queryById(adminId);
        if(admin == null)
            return new StatusResponse(false, "Benutzer nicht gefunden!");
        if(!user.getType().isAdmin())
            return new StatusResponse(false, "Benutzer nicht gefunden!");
        for(Session session : Session.getTable().queryForEq("user_id", admin.getId())){
            if(session.isSU()) {
                session.returnSU();
            }else{
                Session.getTable().delete(session);
            }
        }
        for(Session session : Session.getTable().queryForEq("admin_id", admin.getId())){
            Session.getTable().delete(session);
        }
        User.getTable().delete(admin);
        return new StatusResponse(true, "Der Benutzer wurde gelöscht");
    }

}
