package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.advertal.assest.models.database.*;
import de.advertal.assest.models.web.StatusResponse;
import de.advertal.assest.models.web.WebWording;
import de.advertal.assest.utils.JsonTransformer;
import de.advertal.assest.utils.OrderByValue;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.List;

@SparkController
public class WordingController extends Controller {

    private static Gson gson = new GsonBuilder().create();

    @SparkGet(value = "/wordings/:test")
    public String getWordings(Request request, Response response, @SparkParam("test") int testId){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        Test test = Test.getTable().queryById(testId);
        if(test == null || !test.canWrite(user)){
            response.redirect("/tests", 301);
            return "";
        }
        return getWordings(request, test, null, customerId);
    }

    @SparkGet(value = "/wordings/:test/:skill")
    public String getWordings(Request request, Response response, @SparkParam("test") int testId, @SparkParam("skill") int skillId){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        Test test = Test.getTable().queryById(testId);
        if(test == null || !test.canWrite(user)){
            response.redirect("/tests", 301);
            return "";
        }
        TestSkill testSkill = TestSkill.getTable().builder().where().eq("test_id", testId).and().eq("skill_id", skillId).query().first();
        if(testSkill == null){
            response.redirect("/wordings/"+testId, 301);
            return "";
        }
        Skill skill = testSkill.getSkill();
        return getWordings(request, test, skill, customerId);
    }

    private String getWordings(Request request, Test test, Skill skill, int customerId){
        int skillId = 0;
        String title = test.getName();
        if(skill!=null){
            skillId = skill.getId();
            title += " → "+skill.getName();
        }
        List<Skill> skills = null;
        if(skillId == 0){
            skills = test.getSkills();
        }
        return VIEW(request, "wordings", ME("skills", skills), ME("testId", test.getId()), ME("skillId", skillId), ME("wordings", getWordings(test.getId(), skillId, customerId)), ME("wordingtitle", title));
    }

    private String getWordings(int testId, int skillId, int customerId){
        List<Wording> wordings = Wording.getTable().builder().where().eq("test_id", testId).and().eq("skill_id", skillId).and().eq("customer_id", customerId).query().all();
        if(wordings.size()==0){
            wordings = Wording.getTable().builder().where().eq("test_id", testId).and().eq("skill_id", skillId).and().eq("customer_id", 0).query().all();
        }
        if(wordings.size()==0){
            wordings = Wording.defaultWordings(testId, skillId, customerId);
        }
        List<WebWording> webWordings = new ArrayList<>();
        for(Wording wording : wordings)
            webWordings.add(new WebWording(wording.getMinValue(), wording.getText()));
        webWordings.sort(new OrderByValue());
        return gson.toJson(webWordings);
    }

    @SparkPost(value = "/api/wordings/:test", transformer = JsonTransformer.class)
    public StatusResponse editWordings(Request request, @SparkParam("test") int testId, @SparkBody WebWording[] wordings){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        Test test = Test.getTable().queryById(testId);
        if(test == null || !test.canWrite(user))
            return new StatusResponse(false, "Test nicht gefunden");
        setWordings(testId, 0, customerId, wordings);
        return new StatusResponse(true, "Die Auswertung wurde angepasst");
    }

    @SparkPost(value = "/api/wordings/:test/:skill", transformer = JsonTransformer.class)
    public StatusResponse editWordings(Request request, @SparkParam("test") int testId, @SparkParam("skill") int skillId, @SparkBody WebWording[] wordings){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        Test test = Test.getTable().queryById(testId);
        if(test == null || !test.canWrite(user))
            return new StatusResponse(false, "Test nicht gefunden");
        TestSkill testSkill = TestSkill.getTable().builder().where().eq("test_id", testId).and().eq("skill_id", skillId).query().first();
        if(testSkill == null)
            return new StatusResponse(false, "Skill nicht gefunden");
        setWordings(testId, skillId, customerId, wordings);
        return new StatusResponse(true, "Die Auswertung wurde angepasst");
    }


    private void setWordings(int testId, int skillId, int customerId, WebWording[] wordings){
        List<Wording> dbWordings = Wording.getTable().builder().where().eq("test_id", testId).and().eq("customer_id", customerId).and().eq("skill_id", skillId).query().all();
        while(dbWordings.size()>wordings.length){
            Wording wording = dbWordings.get(dbWordings.size()-1);
            Wording.getTable().delete(wording);
            dbWordings.remove(wording);
        }
        while(dbWordings.size()<wordings.length){
            Wording wording = new Wording();
            wording.setTestId(testId);
            wording.setCustomerId(customerId);
            wording.setSkillId(skillId);
            dbWordings.add(wording);
        }
        for(int i = 0; i < wordings.length; i++){
            Wording wording = dbWordings.get(i);
            WebWording webWording = wordings[i];
            wording.setMinValue(webWording.getMinValue());
            wording.setText(webWording.getText());
        }
        for(Wording wording : dbWordings){
            if(wording.getId() == 0){
                Wording.getTable().create(wording);
            }else{
                Wording.getTable().update(wording);
            }
        }
    }

}
