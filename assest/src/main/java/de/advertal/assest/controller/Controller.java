package de.advertal.assest.controller;

import com.google.gson.*;
import de.advertal.assest.Assest;
import de.advertal.assest.models.database.Session;
import de.advertal.assest.models.database.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import spark.ModelAndView;
import spark.Request;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class Controller {

    private static Gson gson;

    static {
        gson = new GsonBuilder().create();
    }

    protected JsonElement JSON_BODY(String body){
        return new JsonParser().parse(body);
    }

    protected String VIEW(Request request, String view, ModelEntry... entries){
        return Assest.getInstance().getTemplateEngine().render(new ModelAndView(MODEL(request,entries),view+".ftl"));
    }

    protected Map<String,Object> MODEL(Request request, ModelEntry... entries){
        Map<String,Object> model = new HashMap<>();
        Session session = request.attribute("session");
        if(session != null){
            model.put("session", session);
            model.put("user", session.getUser());
        }
        model.put("branding", Assest.getInstance().getBranding());
        model.put("cdn", Assest.getInstance().getCdn());
        for(ModelEntry e : entries)
            model.put(e.getKey(),e.getValue());
        return model;
    }

    protected JsonElement PARSE(String body){
        return new JsonParser().parse(body);
    }

    protected User USER(Request request){
        Session session = request.attribute("session");
        if(session==null)
            return null;
        return session.getUser();
    }

    protected ModelEntry ME(String key, Object value){
        return new ModelEntry(key,value);
    }

    @AllArgsConstructor
    @Getter
    protected static class ModelEntry {
        private String key;
        private Object value;
    }

    public static Validator VALIDATE(String input, Validator... validators){
        for(Validator v : validators)
            if(!v.validate(input))
                return v;
        return null;
    }

    public interface Validator {
        Validator EMAIL = input -> Pattern.compile("^(.+)@(.+)$").matcher(input).matches();
        Validator NAME = input -> Pattern.compile("(([A-Z]?[a-z]+)([-]([A-Z]?[a-z]+)){0,2})([ ](([A-Z]?[a-z]+)([-]([A-Z]?[a-z]+)){0,2})){0,2}").matcher(input).matches();
        Validator PASSWORD = input -> Pattern.compile("[A-Za-z0-9&!?]{8,20}").matcher(input).matches();
        boolean validate(String input);
    }

}
