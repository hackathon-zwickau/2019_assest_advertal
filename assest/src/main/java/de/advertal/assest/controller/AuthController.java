package de.advertal.assest.controller;

import de.advertal.assest.models.database.Session;
import de.advertal.assest.models.database.User;
import com.ftpix.sparknnotation.annotations.*;
import de.advertal.assest.models.web.ChangePasswordRequest;
import de.advertal.assest.models.web.LoginRequest;
import de.advertal.assest.models.web.StatusResponse;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.ArrayList;
import java.util.List;

@SparkController
public class AuthController extends Controller {

    private static final List<String> UNAUTHORIZED_PATHS = new ArrayList<String>(){{
        add("/login");
    }};

    @SparkOptions("/*")
    public String options(Response response){
        return "";
    }

    @SparkBefore("/*")
    public void before(Request request, Response response){
        if(request.requestMethod().equalsIgnoreCase("options")){
            response.header("Access-Control-Allow-Origin", request.headers("Origin"));
            response.header("Access-Control-Allow-Credentials", "true");
            response.header("Access-Control-Allow-Headers", "content-type");
            response.type("text/plain");
            return;
        }
        response.header("Access-Control-Allow-Origin", request.headers("Origin"));
        response.header("Access-Control-Allow-Credentials", "true");
        String token = request.cookie(Session.COOKIE_NAME);
        if(token == null){
            String auth = request.headers("Authorization");
            if(auth!=null){
                if(auth.startsWith("Bearer ")){
                    String[] spl = auth.split(" ");
                    if(spl.length==2){
                        token = spl[1];
                    }
                }
            }
        }
        if(token != null){
            Session session = Session.getTable().builder().where().eq("token", token).query().first();
            if(session!=null){
                if(session.isExpired()){
                    Session.getTable().delete(session);
                    response.removeCookie(Session.COOKIE_NAME);
                }else{
                    if(session.willExpireSoon()){
                        session.refresh();
                        Session.getTable().update(session);
                        session.apply(response);
                    }
                    request.attribute("session", session);
                }
            }
        }
        boolean unauthorized = false;
        if(request.attribute("session") == null){
            if(!UNAUTHORIZED_PATHS.contains(request.pathInfo())){
                unauthorized = true;
            }
            if(request.pathInfo().startsWith("/api/solve/")){
                unauthorized = false;
            }
        }
        if(unauthorized){
            response.redirect("/login", 301);
            Spark.halt(301, "");
            return;
        }
        if(request.pathInfo().startsWith("/admin") && !request.pathInfo().equals("/admin/return")){
            Session session = request.attribute("session");
            if(!session.getUser().getType().isAdmin()){
                response.redirect("/", 301);
                Spark.halt(301, "");
                return;
            }
        }
    }

    @SparkGet(value = "/login")
    public String getLogin(Request request){
        return VIEW(request, "login");
    }

    @SparkGet("/")
    public String getIndex(Response response){
        response.redirect("/tests", 301);
        return "";
    }

    @SparkPost(value = "/password", transformer = JsonTransformer.class)
    public StatusResponse postPasswordChange(Request request, @SparkBody ChangePasswordRequest change){
        User user = USER(request);
        if(!change.getPassword().equals(change.getRepeat()))
            return new StatusResponse(false, "Die Kennwörter stimmen nicht überein");
        if(change.getPassword().length() < 8 || change.getPassword().length() > 20)
            return new StatusResponse(false, "Ungültiges Kennwort");
        user.setPassword(change.getPassword());
        User.getTable().update(user);
        for(Session session : Session.getTable().builder().where().eq("user_id", user.getId()).and().eq("admin_id", 0).query().all()){
            Session.getTable().delete(session);
        }
        for(Session session : Session.getTable().builder().where().eq("admin_id", user.getId()).query().all()){
            Session.getTable().delete(session);
        }
        return new StatusResponse(true, "Kennwort erfolgreich geändert!");
    }

    @SparkPost(value = "/login", transformer = JsonTransformer.class)
    public StatusResponse postLogin(@SparkBody LoginRequest login, Response response){
        User user = User.getTable().builder().where().eq("email", login.getEmail()).query().first();
        if(user == null)
            return new StatusResponse(false, "Anmeldung fehlgeschlagen!");
        if(user.getType().isUser())
            return new StatusResponse(false, "Anmeldung fehlgeschladen!");
        if(!user.checkPassword(login.getPassword()))
            return new StatusResponse(false, "Anmeldung fehlgeschlagen!");
        Session session = new Session();
        session.setType(Session.Type.WEB);
        session.setUserId(user.getId());
        session.setKeepSession(login.isKeepLogin());
        session.generateToken();
        session.refresh();
        Session.getTable().create(session);
        session.apply(response);
        return new StatusResponse(true, "Erfolgreich angemeldet!");
    }

    @SparkPost(value = "/logout", transformer = JsonTransformer.class)
    public StatusResponse postLogout(Request request, Response response){
        Session session = request.attribute("session");
        if(session==null)
            Session.getTable().delete(session);
        response.removeCookie(Session.COOKIE_NAME);
        return new StatusResponse(true, "Erfolgreich abgemeldet!");
    }

}
