package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.*;
import de.advertal.assest.models.database.Category;
import de.advertal.assest.models.database.User;
import de.advertal.assest.models.web.CategoryCreateRequest;
import de.advertal.assest.models.web.StatusResponse;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;

@SparkController
public class CategoryController extends Controller {

    @SparkGet(value = "/admin/categories")
    public String getCategories(Request request){
        return VIEW(request, "admin/categories", ME("categories", Category.getTable().queryForAll()));
    }

    @SparkPost(value = "/api/categories", transformer = JsonTransformer.class)
    public StatusResponse createCategory(Request request, @SparkBody CategoryCreateRequest create){
        User user = USER(request);
        if(!user.getType().isAdmin())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung");
        Category category = Category.getTable().builder().where().eq("parent_id", create.getParentId()).and().eq("name", create.getName()).query().first();
        if(category!=null)
            return new StatusResponse(false, "Es existiert bereits eine Kategorie mit diesem Namen");
        if(create.getParentId()>0)
            if(Category.getTable().queryById(create.getParentId())==null)
                return new StatusResponse(false, "Die Oberkategorie existiert nicht");
        category = new Category();
        category.setCustomerId(0);
        category.setParentId(create.getParentId());
        category.setName(create.getName());
        Category.getTable().create(category);
        return new StatusResponse(true, "Die Kategorie wurde erstellt");
    }

    @SparkDelete(value = "/api/categories/:category", transformer = JsonTransformer.class)
    public StatusResponse deleteCategory(Request request, @SparkParam("category") int categoryId){
        User user = USER(request);
        if(!user.getType().isAdmin())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung");
        Category category = Category.getTable().queryById(categoryId);
        if(category==null)
            return new StatusResponse(false, "Die Kategorie wurde nicht gefunden");
        category.delete();
        return new StatusResponse(true, "Die Kategorie wurde gelöscht");
    }

}
