package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.SparkController;
import com.ftpix.sparknnotation.annotations.SparkGet;
import com.ftpix.sparknnotation.annotations.SparkParam;
import de.advertal.assest.models.database.*;
import de.advertal.assest.models.web.TestSkillResult;
import de.advertal.assest.utils.Helper;
import spark.Request;
import spark.Response;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@SparkController
public class EvaluationController extends Controller {

    @SparkGet("/auswertung/run/:run")
    public String getRunEvaluation(Request request, Response response, @SparkParam("run") int runId){
        User user = USER(request);
        TestRun run = TestRun.getTable().queryById(runId);
        if(run==null || !run.canRead(user)){
            response.redirect("/tests", 301);
            return "";
        }
        if(run.getState() != ResultState.DONE){
            response.redirect("/tests/"+run.getTestId(), 301);
            return "";
        }
        TestResult result = TestResult.getTable().builder().where().eq("test_run_id", run.getId()).query().first();
        List<SkillResult> skillResults = SkillResult.getTable().builder().where().eq("test_run_id", run.getId()).query().all();
        return VIEW(request, "evaluation-single", ME("run", run), ME("testresult", result.isItems()?result:null), ME("skillresults", skillResults));
    }

    @SparkGet("/auswertung/test/:test")
    public String getTestEvaluation(Request request, Response response, @SparkParam("test") int testId){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        Test test = Test.getTable().queryById(testId);
        if(test==null || !test.canRead(user)){
            response.redirect("/tests", 301);
            return "";
        }
        double valueTest = 0;
        Map<Integer, Double> valueSkills = new HashMap<>();
        List<TestRun> runs;
        if(user.getType().isAdmin()){
            runs = TestRun.getTable().builder().where().eq("test_id", test.getId()).and().eq("state", "DONE").query().all();
        }else{
            runs = TestRun.getTable().builder().where().eq("test_id", test.getId()).and().eq("customer_id", user.getId()).and().eq("state", "DONE").query().all();
        }
        for(TestRun run : runs){
            TestResult result = TestResult.getTable().builder().where().eq("test_run_id", run.getId()).query().first();
            valueTest += result.getValue();
            for(SkillResult skillResult : SkillResult.getTable().builder().where().eq("test_run_id", run.getId()).query().all()){
                if(!valueSkills.containsKey(skillResult.getSkillId()))
                    valueSkills.put(skillResult.getSkillId(), 0d);
                double value = valueSkills.get(skillResult.getSkillId());
                value += skillResult.getValue();
                valueSkills.put(skillResult.getSkillId(), value);
            }
        }
        valueTest = (valueTest / runs.size()) * 100d;
        List<TestSkillResult> skillResults = new ArrayList<>();
        int items = 0;
        for(int skillId : valueSkills.keySet()) {
            Skill skill = Skill.getTable().queryById(skillId);
            if(!skill.isSoft())
                items++;
            skillResults.add(new TestSkillResult(testId, customerId, skill, valueSkills.get(skillId) / runs.size()));
        }
        return VIEW(request, "evaluation-test", ME("test", test), ME("testValue", items>0?String.valueOf(valueTest):null), ME("skillresults", skillResults));
    }

}
