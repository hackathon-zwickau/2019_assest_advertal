package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.*;
import de.advertal.assest.models.database.Category;
import de.advertal.assest.models.database.Skill;
import de.advertal.assest.models.database.User;
import de.advertal.assest.models.web.*;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;

@SparkController
public class SkillController extends Controller {

    @SparkGet(value = "/skills")
    public String getSkills(Request request){
        return VIEW(request, "skills", ME("categories", Category.getTable().queryForAll()));
    }

    @SparkPost(value = "/api/skills", transformer = JsonTransformer.class)
    public StatusResponse createSkill(Request request, @SparkBody SkillCreateRequest create){
        User user = USER(request);
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        Skill skill = Skill.getTable().builder().where().eq("category_id", create.getCategoryId()).and().eq("name", create.getName()).and().c(" (").eq("customer_id", customerId).or().eq("customer_id", 0).c(")").query().first();
        if(skill!=null)
            return new StatusResponse(false, "Es existiert bereits ein Skill mit diesem Namen");
        if(create.getCategoryId()>0)
            if(Category.getTable().queryById(create.getCategoryId())==null)
                return new StatusResponse(false, "Die Kategorie existiert nicht");
        skill = new Skill();
        skill.setCustomerId(customerId);
        skill.setCategoryId(create.getCategoryId());
        skill.setDescription(create.getDescription());
        skill.setSoft(create.isSoft());
        skill.setName(create.getName());
        Skill.getTable().create(skill);
        return new StatusResponse(true, "Der Skill wurde erstellt");
    }

    @SparkDelete(value = "/api/skills/:skill", transformer = JsonTransformer.class)
    public StatusResponse deleteSkill(Request request, @SparkParam("skill") int skillId){
        User user = USER(request);
        Skill skill = Skill.getTable().queryById(skillId);
        if(skill==null)
            return new StatusResponse(false, "Der Skill wurde nicht gefunden");
        if(!user.getType().isAdmin() && skill.getCustomerId() != user.getId())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung");
        skill.delete();
        return new StatusResponse(true, "Der Skill wurde gelöscht");
    }

    @SparkGet(value = "/api/skills/:skill", transformer = JsonTransformer.class)
    public EditSkillResponse getEditSkill(Request request, @SparkParam("skill") int skillId){
        User user = USER(request);
        Skill skill = Skill.getTable().queryById(skillId);
        if(skill==null)
            return new EditSkillResponse(false, "Der Skill wurde nicht gefunden");
        if(!user.getType().isAdmin() && skill.getCustomerId() != user.getId())
            return new EditSkillResponse(false, "Dafür haben Sie keine Berechtigung");
        return new EditSkillResponse(true, "Der Skill wurde gefunden", skill);
    }

    @SparkPost(value = "/api/skills/:skill", transformer = JsonTransformer.class)
    public StatusResponse updateSkill(Request request, @SparkParam("skill") int skillId, @SparkBody UpdateSkillRequest update){
        User user = USER(request);
        Skill skill = Skill.getTable().queryById(skillId);
        if(skill==null)
            return new StatusResponse(false, "Der Skill wurde nicht gefunden");
        if(!user.getType().isAdmin() && skill.getCustomerId() != user.getId())
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung");
        skill.setName(update.getName());
        skill.setDescription(update.getDescription());
        skill.setCategoryId(update.getCategory());
        skill.setSoft(update.isSoft());
        Skill.getTable().update(skill);
        return new StatusResponse(true, "Der Skill wurde bearbeitet");
    }

}
