package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.SparkController;
import com.ftpix.sparknnotation.annotations.SparkDelete;
import com.ftpix.sparknnotation.annotations.SparkGet;
import com.ftpix.sparknnotation.annotations.SparkParam;
import de.advertal.assest.models.database.Test;
import de.advertal.assest.models.database.User;
import de.advertal.assest.models.web.StatusResponse;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;
import spark.Response;
import java.util.ArrayList;
import java.util.List;

@SparkController
public class TestController extends Controller {

    @SparkGet(value = "/tests")
    public String getIndex(Request request){
        return VIEW(request, "tests");
    }

    @SparkGet(value = "/tests/:test")
    public String getTest(Request request, Response response, @SparkParam("test") int testId){
        User user = USER(request);
        Test test = Test.getTable().queryById(testId);
        if(test == null){
            response.redirect("/tests");
            return "";
        }
        if(!test.canRead(user)){
            response.redirect("/tests");
            return "";
        }
        if(user.getType().isAdmin()){
            if(test.getCustomerId() == 0){
                List<User> customers = User.getTable().queryForEq("type", "CUSTOMER");
                List<User> subusers = new ArrayList<>();
                if(customers.size()>0)
                    subusers = User.getTable().queryForEq("parent_id", customers.get(0).getId());
                return VIEW(request, "test", ME("test", test), ME("customers", customers), ME("subusers", subusers));
            }else{
                return VIEW(request, "test", ME("test", test), ME("subusers", User.getTable().queryForEq("parent_id", test.getCustomerId())));
            }
        }else{
            return VIEW(request, "test", ME("test", test), ME("subusers", User.getTable().queryForEq("parent_id", user.getId())));
        }
    }
    @SparkDelete(value = "/api/tests/:test", transformer = JsonTransformer.class)
    public StatusResponse deleteTest(Request request, @SparkParam("test") int testId){
        User user = USER(request);
        Test test = Test.getTable().queryById(testId);
        if(test==null)
            return new StatusResponse(false, "Der Test wurde nicht gefunden");
        if(!test.canWrite(user))
            return new StatusResponse(false, "Dafür haben Sie keine Berechtigung");
        test.delete();
        return new StatusResponse(true, "Der Test wurde gelöscht");
    }


}
