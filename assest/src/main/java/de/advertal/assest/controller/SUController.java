package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.SparkController;
import com.ftpix.sparknnotation.annotations.SparkGet;
import com.ftpix.sparknnotation.annotations.SparkParam;
import de.advertal.assest.models.database.Session;
import de.advertal.assest.models.database.User;
import spark.Request;
import spark.Response;

@SparkController
public class SUController extends Controller {

    @SparkGet("/admin/login/:user")
    public String getLogin(Request request, Response response, @SparkParam("user") int userId){
        User user = User.getTable().queryById(userId);
        if(user!=null){
            Session session = request.attribute("session");
            if(session!=null){
                if(session.getUser().getType().isAdmin()){
                    session.setSU(user.getId());
                    Session.getTable().update(session);
                }
            }
        }
        response.redirect("/", 301);
        return "";
    }

    @SparkGet("/admin/return")
    public String getReturn(Request request, Response response){
        Session session = request.attribute("session");
        if(session!=null){
            session.returnSU();
            Session.getTable().update(session);
        }
        response.redirect("/", 301);
        return "";
    }

}
