package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.*;
import com.google.gson.JsonObject;
import de.advertal.assest.models.database.*;
import de.advertal.assest.models.web.SolvableItem;
import de.advertal.assest.models.web.SolvableResponse;
import de.advertal.assest.models.web.StatusResponse;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@SparkController
public class ApiController extends Controller {

    @SparkGet(value = "/api/subusers/:customer", transformer = JsonTransformer.class)
    public List<User> getSubUsers(Request request, @SparkParam("customer") int customerId){
        User user = USER(request);
        if(!user.getType().isAdmin())
            return new ArrayList<>();
        return User.getTable().queryForEq("parent_id", customerId);
    }

    @SparkPost(value = "/api/runs", transformer = JsonTransformer.class)
    public StatusResponse createTestRun(Request request){
        User user = USER(request);
        JsonObject body = PARSE(request.body()).getAsJsonObject();
        int customerId = user.getId();
        int testId = body.get("test_id").getAsInt();
        Test test = Test.getTable().queryById(testId);
        if(test == null || !test.canRead(user))
            return new StatusResponse(false, "Sie haben keinen Zugriff auf diesen Test!");
        int userId = body.get("user_id").getAsInt();
        if(user.getType().isAdmin())
            customerId = test.getCustomerId()!=0?test.getCustomerId():body.get("customer_id").getAsInt();
        User candidate = User.getTable().queryById(userId);
        if(candidate == null || candidate.getParentId() != customerId)
            return new StatusResponse(false, "Der Bewerber wurde nicht gefunden!");
        if(!test.canRead(User.getTable().queryById(customerId)))
            return new StatusResponse(false, "Der Kunde hat keinen Zugriff auf diesen Test!");
        TestRun run = new TestRun();
        run.setCreatedAt(Timestamp.from(Instant.now()));
        run.setSolvedAt(Timestamp.from(Instant.now()));
        run.setCustomerId(customerId);
        run.setTestId(testId);
        run.setUserId(userId);
        run.generateRandom();
        run.generateToken();
        run.setState(ResultState.CREATED);
        TestRun.getTable().create(run);
        return new StatusResponse(true, "Der Durchlauf wurde erstellt.");
    }

    @SparkDelete(value = "/api/runs/:run", transformer = JsonTransformer.class)
    public StatusResponse deleteRun(Request request, @SparkParam("run") int runId){
        User user = USER(request);
        TestRun run = TestRun.getTable().queryById(runId);
        if(run == null || !run.canWrite(user))
            return new StatusResponse(false, "Sie haben keinen Zugriff auf diesen Durchlauf!");
        run.delete();
        return new StatusResponse(true, "Der Durchlauf wurde gelöscht.");
    }

    @SparkGet(value = "/api/solve/run/:run", transformer = JsonTransformer.class)
    public SolvableResponse getSolve(Request request, @SparkParam("run") String runToken){
        TestRun run = TestRun.getTable().builder().where().eq("token", runToken).query().first();
        if(run == null)
            return new SolvableResponse(false, 0);
        List<Item> items = run.getItems();
        if(run.getCurrent()>items.size()-1){
            return new SolvableResponse(false, 100, run.getTest().getName());
        }
        double percentage = ((double) run.getCurrent() / (double)items.size())*100d;
        return new SolvableResponse(true, percentage, run.getTest().getName(),new SolvableItem(items.get(run.getCurrent())));
    }

    @SparkPost(value = "/api/solve/run/:run", transformer = JsonTransformer.class)
    public SolvableResponse postSolve(Request request, @SparkParam("run") String runToken){
        TestRun run = TestRun.getTable().builder().where().eq("token", runToken).query().first();
        if(run == null)
            return new SolvableResponse(false, 0);
        List<Item> items = run.getItems();
        if(run.getCurrent()>items.size()-1){
            return new SolvableResponse(false, 100, run.getTest().getName());
        }
        String answer = request.body();
        Item currentItem = items.get(run.getCurrent());
        double percentage = ((double) run.getCurrent() / (double)items.size())*100d;
        if(!currentItem.validateAnswer(answer))
            return new SolvableResponse(false, percentage);
        Answer answerObject = new Answer();
        answerObject.setItemId(currentItem.getId());
        answerObject.setTestRunId(run.getId());
        answerObject.setValue(answer);
        Answer.getTable().create(answerObject);
        run.setCurrent(run.getCurrent()+1);
        if(run.getCurrent()>items.size()-1){
            run.setSolvedAt(Timestamp.from(Instant.now()));
            run.setState(ResultState.SUBMITTED);
            TestRun.getTable().update(run);
            return new SolvableResponse(true, 100, run.getTest().getName());
        }
        TestRun.getTable().update(run);
        percentage = ((double) run.getCurrent() / (double)items.size())*100d;
        return new SolvableResponse(true, percentage, run.getTest().getName(), new SolvableItem(items.get(run.getCurrent())));
    }

}
