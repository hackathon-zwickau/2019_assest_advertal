package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.SparkController;
import com.ftpix.sparknnotation.annotations.SparkGet;
import com.ftpix.sparknnotation.annotations.SparkParam;
import de.advertal.assest.models.database.*;
import de.advertal.assest.utils.DataTable;
import de.advertal.assest.utils.Helper;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SparkController
public class TableController extends Controller {

    @SparkGet(value = "/table/tests/all", transformer = JsonTransformer.class)
    public DataTable getAllTests(Request request){
        User user = USER(request);
        DataTable table = new DataTable();
        if(user.getType().isUser()){
            return table;
        }else
        if(user.getType().isAdmin()){
            for(Test test : Test.getTable().queryForAll()){
                table.row(testLink(test), customerName(test.getCustomerId()), "<button class=\"btn btn-danger btn-xs\" onclick=\"deleteTest("+test.getId()+")\">Löschen</button>", Helper.formatDate(Date.from(test.getCreatedAt().toInstant())));
            }
        }else
        if(user.getType().isCustomer()){
            for(Test test : Test.getTable().builder().where().eq("customer_id", 0).or().eq("customer_id", user.getId()).query().all()){
                table.row(testLink(test), customerType(test.getCustomerId()), user.getId()==test.getCustomerId()?"<button class=\"btn btn-danger btn-xs\" onclick=\"deleteTest("+test.getId()+")\">Löschen</button>":"" ,Helper.formatDate(Date.from(test.getCreatedAt().toInstant())));
            }
        }
        return table;
    }

    @SparkGet(value = "/table/candidates/all", transformer = JsonTransformer.class)
    public DataTable getAllCandidates(Request request){
        User user = USER(request);
        DataTable table = new DataTable();
        List<User> candidates = new ArrayList<>();
        if(user.getType().isAdmin()){
            candidates = User.getTable().queryForEq("type", "USER");
        }else if(user.getType().isCustomer()){
            candidates = User.getTable().builder().where().eq("type", "USER").and().eq("parent_id", user.getId()).query().all();
        }
        for(User candidate : candidates){
            if(user.getType().isAdmin()){
                User customer = candidate.getParent();
                table.row(
                        candidate.getFirstName()+" "+candidate.getLastName(),
                        customer.getFirstName()+" "+customer.getLastName(),
                        "<button class=\"btn btn-danger btn-xs\" onclick=\"deleteUser("+candidate.getId()+")\">Löschen</button>", //"<a class=\"btn btn-warning btn-xs\" href=\"/admin/login/"+candidate.getId()+"\">Anmelden</a>"
                        Helper.formatDate(candidate.getCreatedAt())
                );
            }else{
                table.row(
                        candidate.getFirstName()+" "+candidate.getLastName(),
                        "<button class=\"btn btn-danger btn-xs\" onclick=\"deleteUser("+candidate.getId()+")\">Löschen</button>",
                        Helper.formatDate(candidate.getCreatedAt())
                );
            }
        }
        return table;
    }

    @SparkGet(value = "/table/customers/all", transformer = JsonTransformer.class)
    public DataTable getAllCustomers(Request request){
        User user = USER(request);
        DataTable table = new DataTable();
        if(!user.getType().isAdmin())
            return table;
        for(User customer : User.getTable().queryForEq("type", "CUSTOMER")){
            table.row(
                    customer.getFirstName()+" "+customer.getLastName(),
                    "<button class=\"btn btn-danger btn-xs\" onclick=\"deleteUser("+customer.getId()+")\">Löschen</button> <a class=\"btn btn-warning btn-xs\" href=\"/admin/login/"+customer.getId()+"\">Anmelden</a>",
                    Helper.formatDate(customer.getCreatedAt())
            );
        }
        return table;
    }

    @SparkGet(value = "/table/admins/all", transformer = JsonTransformer.class)
    public DataTable getAllAdmins(Request request){
        User user = USER(request);
        DataTable table = new DataTable();
        if(!user.getType().isAdmin())
            return table;
        for(User admin : User.getTable().queryForEq("type", "ADMIN")){
            table.row(
                    admin.getFirstName()+" "+admin.getLastName(),
                    "<button class=\"btn btn-danger btn-xs\" onclick=\"deleteUser("+admin.getId()+")\">Löschen</button> <a class=\"btn btn-warning btn-xs\" href=\"/admin/login/"+admin.getId()+"\">Anmelden</a>",
                    Helper.formatDate(admin.getCreatedAt())
            );
        }
        return table;
    }

    @SparkGet(value = "/table/categories/all", transformer = JsonTransformer.class)
    public DataTable getAllCategories(Request request){
        User user = USER(request);
        DataTable table = new DataTable();
        if(!user.getType().isAdmin())
            return table;
        for(Category category : Category.getTable().queryForAll()){
            Category parent = category.getParent();
            table.row(
                    category.getName(),
                    parent!=null?parent.getName():"Nicht zugewiesen",
                    "<button class=\"btn btn-danger btn-xs\" onclick=\"deleteCategory("+category.getId()+")\">Löschen</button>"
            );
        }
        return table;
    }

    @SparkGet(value = "/table/skills/all", transformer = JsonTransformer.class)
    public DataTable getAllSkills(Request request){
        User user = USER(request);
        DataTable table = new DataTable();
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        for(Skill skill : Skill.getTable().queryForEq("customer_id", customerId)){
            Category category = skill.getCategory();
            table.row(
                    skill.getName(),
                    category!=null?category.getName():"Nicht zugewiesen",
                    "<button class=\"btn btn-warning btn-xs\" onclick=\"editSkill("+skill.getId()+")\">Bearbeiten</button> <button class=\"btn btn-danger btn-xs\" onclick=\"deleteSkill("+skill.getId()+")\">Löschen</button>"
            );
        }
        return table;
    }

    @SparkGet(value = "/table/items/all", transformer = JsonTransformer.class)
    public DataTable getAllItems(Request request){
        User user = USER(request);
        DataTable table = new DataTable();
        int customerId = user.getId();
        if(user.getType().isAdmin())
            customerId = 0;
        for(Item item : Item.getTable().queryForEq("customer_id", customerId)){
            Skill skill = item.getSkill();
            table.row(
                    item.getQuestion(),
                    skill.getName(),
                    item.getType().getDisplay(),
                    "<button class=\"btn btn-warning btn-xs\" onclick=\"editItem("+item.getId()+")\">Bearbeiten</button> <button class=\"btn btn-danger btn-xs\" onclick=\"deleteItem("+item.getId()+")\">Löschen</button>"
            );
        }
        return table;
    }

    @SparkGet(value = "/table/tests/test/:test/runs", transformer = JsonTransformer.class)
    public DataTable getAllTestRuns(Request request, @SparkParam("test") int testId){
        User user = USER(request);
        DataTable table = new DataTable();
        Test test = Test.getTable().queryById(testId);
        if(test == null)
            return table;
        if(!test.canRead(user))
            return table;
        List<TestRun> runs = null;
        if(user.getType().isAdmin()){
            runs = TestRun.getTable().queryForEq("test_id", test.getId());
        }
        if(user.getType().isCustomer()){
            runs = TestRun.getTable().builder().where().eq("test_id", test.getId()).and().eq("customer_id", user.getId()).query().all();
        }
        if(runs!=null){
            for(TestRun run : runs){
                User runUser = run.getUser();
                table.row(
                        runUser.getFirstName()+" "+runUser.getLastName(),
                        displayRunStatus(run.getState()),
                        run.getToken()+" <button class=\"btn btn-info btn-xs\" onclick=\"copyToClipboard('"+run.getToken()+"')\">Kopieren</button>",
                        (run.getState() == ResultState.DONE ? "<a class=\"btn btn-success btn-xs\" href=\"/auswertung/run/"+run.getId()+"\">Auswertung</a>" : "<button class=\"btn btn-danger btn-xs\" onclick=\"deleteRun("+run.getId()+")\">Löschen</button>"),
                        Helper.formatDate(Date.from(run.getCreatedAt().toInstant()))
                );
            }
        }
        return table;
    }

    private String displayRunStatus(ResultState state){
        switch (state) {
            case CREATED:
                return "Noch nicht absolviert";
            case SUBMITTED:
                return "Warte auf Auswertung";
            case PENDING:
                return "Auswertung läuft";
            case DONE:
                return "Auswertung abgeschlossen";
            case FAILED:
                return "Auswertung fehlgeschlagen";
            default:
                return "Unbekannter Status";
        }
    }

    private String testLink(Test test){
        return "<a href=\""+test.getUrl()+"\">"+test.getName()+"</a>";
    }

    private String customerName(int id){
        if(id > 0){
            User user = User.getTable().queryById(id);
            return user.getGender().getTitle()+" "+user.getFirstName()+" "+user.getLastName();
        }
        return "Öffentlich";
    }

    private String customerType(int id){
        if(id > 0)
            return "Eigener Test";
        return "Öffentlich";
    }

}
