package de.advertal.assest.controller;

import com.ftpix.sparknnotation.annotations.*;
import de.advertal.assest.models.database.*;
import de.advertal.assest.models.web.TestCreationRequest;
import de.advertal.assest.models.web.TestCreationResponse;
import de.advertal.assest.utils.JsonTransformer;
import spark.Request;
import spark.Response;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@SparkController
public class WizardController extends Controller {

    @SparkGet(value = "/wizard")
    public String getIndex(Request request){
        return VIEW(request, "wizard", ME("displayCategories", renderCategories()));
    }

    @SparkGet(value = "/api/skills/category/:category", transformer = JsonTransformer.class)
    public List<Skill> getItems(Request request, @SparkParam("category") int categoryId){
        User user = USER(request);
        List<Skill> skills;
        if(categoryId == -1)
            skills = Skill.getTable().builder().where().eq("customer_id", 0).or().eq("customer_id", user.getId()).query().all();
        else {
            skills = Skill.getTable().builder().where().c("(").eq("customer_id", 0).or().eq("customer_id", user.getId()).c(")").and().eq("category_id", categoryId).query().all();
        }
        skills.forEach(Skill::checkAmount);
        return skills;
    }

    @SparkPost(value = "/api/tests", transformer = JsonTransformer.class)
    public TestCreationResponse createTest(Request request, Response response, @SparkBody TestCreationRequest creation){
        User user = USER(request);
        int customerId = user.getType().isAdmin()?0:user.getId();
        if(creation.getName().length()<3){
            return new TestCreationResponse(false, "Der Name muss mindestens 3 Zeichen lang sein.");
        }
        Test exTest = Test.getTable().builder().where().eq("name", creation.getName()).and().c(" (").eq("customer_id", customerId).or().eq("customer_id", 0).c(")").query().first();
        if(exTest!=null){
            return new TestCreationResponse(false, "Es existiert bereits ein Test mit diesem Namen.");
        }
        List<Skill> skills = new ArrayList<>();
        for(int skillId : creation.getSkills()){
            Skill skill = Skill.getTable().queryById(skillId);
            if(skill==null)
                continue;
            if(!skill.canRead(user))
                continue;
            if(!skills.contains(skill))
                skills.add(skill);
        }
        if(skills.size()<1){
            return new TestCreationResponse(false, "Der Test muss mindestens einen Skill enthalten.");
        }
        Test test = new Test();
        test.setCustomerId(customerId);
        test.setCreatedAt(Timestamp.from(Instant.now()));
        test.setName(creation.getName());
        test.setDescription(creation.getDescription());
        Test.getTable().create(test);
        test = Test.getTable().builder().order("id", true).limit(1).query().first();
        Wording.defaultWordings(test.getId(), 0, customerId).forEach(wording -> Wording.getTable().create(wording));
        for(Skill skill : skills){
            TestSkill rel = new TestSkill();
            rel.setTestId(test.getId());
            rel.setSkillId(skill.getId());
            rel.setWeight(1);
            TestSkill.getTable().create(rel);
            Wording.defaultWordings(test.getId(), skill.getId(), customerId).forEach(wording -> Wording.getTable().create(wording));
        }
        return new TestCreationResponse(true, "Test wurde erstellt.", test.getId());
    }

    private String renderCategories(){
        StringBuilder sb = new StringBuilder();
        for(Category sub : Category.getTable().queryForEq("parent_id", 0))
            sb.append(renderCategories(sub, 0));
        return sb.toString();
    }

    private String renderCategories(Category category, int level){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<level-1;i++)
            sb.append("&emsp;");
        if(level>0)
            sb.append("↳ ");
        sb.append("<span class=\"btn-set-category\" onclick=\"wizard.selection.setCategory("+category.getId()+")\">"+category.getName()+"</span><br>");
        for(Category sub : Category.getTable().queryForEq("parent_id", category.getId()))
            sb.append(renderCategories(sub, level+1));
        return sb.toString();
    }

}
