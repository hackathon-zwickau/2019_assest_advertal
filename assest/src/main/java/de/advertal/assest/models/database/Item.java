package de.advertal.assest.models.database;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.advertal.assest.utils.PythonFormatChecker;
import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@DatabaseTable("items")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Item {

    private static final Gson gson = new GsonBuilder().create();

    @Getter @Setter
    static Table<Item, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField
    Type type;
    @DatabaseField(columnName = "customer_id")
    int customerId;
    @DatabaseField(columnName = "skill_id")
    int skillId;
    @DatabaseField
    String question;
    @DatabaseField(columnName = "media_type")
    MediaType mediaType = MediaType.NONE;
    @DatabaseField
    String media = "";
    @DatabaseField(columnName = "time_limit")
    int timeLimit = 0;
    @DatabaseField
    double weight = 1;
    @DatabaseField
    boolean invert = false;
    @DatabaseField
    String answers = "";
    @DatabaseField(columnName = "correct")
    String correctAnswer = "";

    public double assertAnswer(String answer){
        if(type.likert()!=-1){
            short answerValue = Short.parseShort(answer);
            double value = (answerValue-1)*(1d/(type.likert()-1));
            if(value>0.999)
                value = 1;
            if(value<0.001)
                value = 0;
            if(!invert)
                value = 1-value;
            return value;
        }
        if(type.choice()!=-1){
            int maxAnswerCount = gson.fromJson(this.answers, String[].class).length;
            List<Integer> answers = parseIntListAnswer(answer);
            List<Integer> correctAnswers = parseIntListAnswer(correctAnswer);
            double value = 0;
            double valuePerCorrect = 1d / maxAnswerCount;
            for(int i = 1; i <= maxAnswerCount; i++){
                if(correctAnswers.contains(i) && answers.contains(i))
                    value += valuePerCorrect;
                if(!correctAnswers.contains(i) && !answers.contains(i))
                    value += valuePerCorrect;
            }
            if(type.choice()==1)
                value = value > 0.999 ? 1 : 0;
            return value;
        }
        if(type == Type.PYTHON){
            if(answer.length() < 3){
                return 0;
            }
            PythonFormatChecker check = new PythonFormatChecker(answer.split("\n"));
            List<Integer> correctAnswers = parseIntListAnswer(correctAnswer);
            double totalDif = calcDiff(check.totalLength - check.commentLength, correctAnswers.get(0));
            double commentDif = calcDiff((check.commentLength / check.totalLength) * 100, correctAnswers.get(1));
            double fnDif = 0;
            if(check.functionCount != 0){
                fnDif = calcDiff(check.functionNameLengthTotal / check.functionCount, correctAnswers.get(2));
            }
            double varDif = 0;
            if(check.variableCount != 0){
                varDif = calcDiff(check.variableNameLengthTotal / check.variableCount, correctAnswers.get(3));
            }
            double dif = ((totalDif * 2) + (commentDif * 2) + fnDif + varDif) / 6;
            return 1-dif;
        }
        return 0;
    }

    private int calcDiff(int a, int c){
        if(c == 0)
            return 1;
        int lengthDiff = Math.max(c, a) - Math.min(c, a);
        double lengthDiffPerc = lengthDiff / c;
        if(lengthDiff > 1){
            lengthDiff = 1;
        }
        return lengthDiff;
    }

    public boolean validateAnswer(String answer){
        if(type.likert()!=-1){
            if(answer.length()!=1)
                return false;
            if(!Character.isDigit(answer.charAt(0)))
                return false;
            short value = Short.parseShort(answer);
            if(value<1 || value>type.likert())
                return false;
            return true;
        }
        if(type.choice()!=-1){
            int maxAnswerCount = gson.fromJson(this.answers, String[].class).length;
            List<Integer> set = new ArrayList<>();
            String[] answers = answer.split(",");
            if(answers.length > maxAnswerCount)
                return false;
            for(String choice : answers){
                int value = parseIntAnswer(choice);
                if(value<1 || value > maxAnswerCount)
                    return false;
                if(set.contains(value))
                    return false;
                set.add(value);
            }
            if(type.choice()==1){
                if(set.size()!=1)
                    return false;
                return true;
            }else{
                return true;
            }
        }
        if(type == Type.PYTHON){
            return true;
        }
        return false;
    }

    private List<Integer> parseIntListAnswer(String answer){
        List<Integer> list = new ArrayList<>();
        for(String s : answer.split(","))
            list.add(parseIntAnswer(s));
        return list;
    }

    private int parseIntAnswer(String answer){
        try {
            return Integer.parseInt(answer);
        }catch (Exception ex){}
        return -1;
    }

    @AllArgsConstructor
    @Getter
    public enum Type {
        SINGLE_CHOICE("Single Choice"),
        MULTIPLE_CHOICE("Multiple Choice"),
        LIKERT4("Likert 4"),
        LIKERT5("Likert 5"),
        PYTHON("Python Codestyle");
        private String display;
        public int likert(){
            switch (this){
                case LIKERT4:
                    return 4;
                case LIKERT5:
                    return 5;
            }
            return -1;
        }
        public int choice(){
            switch (this){
                case SINGLE_CHOICE:
                    return 1;
                case MULTIPLE_CHOICE:
                    return 2;
            }
            return -1;
        }
    }

    public enum MediaType {
        NONE,
        IMAGE,
        AUDIO,
        VIDEO
    }

    public User getCustomer(){
        if(customerId < 1)
            return null;
        return User.getTable().queryById(customerId);
    }

    public boolean isShared(){
        return getCustomerId() < 1;
    }

    public Skill getSkill(){
        return Skill.getTable().queryById(skillId);
    }

    public void delete(){
        getTable().delete(this);
    }

}
