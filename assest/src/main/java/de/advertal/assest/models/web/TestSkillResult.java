package de.advertal.assest.models.web;

import de.advertal.assest.models.database.Skill;
import de.advertal.assest.models.database.Wording;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TestSkillResult {
    int testId;
    int customerId;
    Skill skill;
    double value;
    public String getValueInPercent(){
        return String.valueOf(value * 100d);
    }
    public Wording getWording(){
        return Wording.findWording(testId, skill.getId(), customerId, getValue());
    }
}
