package de.advertal.assest.models.database;

public interface Accessable {

    boolean canRead(User user);
    boolean canWrite(User user);

}
