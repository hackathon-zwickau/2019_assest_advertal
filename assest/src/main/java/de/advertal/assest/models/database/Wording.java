package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@DatabaseTable("wordings")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Wording {

    @Getter @Setter
    static Table<Wording, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "customer_id")
    int customerId;
    @DatabaseField(columnName = "test_id")
    int testId;
    @DatabaseField(columnName = "skill_id")
    int skillId;
    @DatabaseField(columnName = "min_value")
    double minValue;
    @DatabaseField
    String text;

    public static List<Wording> defaultWordings(int testId, int skillId, int customerId){
        List<Wording> wordings = new ArrayList<>();
        Wording wording = new Wording();
        wording.setMinValue(0);
        wording.setText("Der Nutzer weist in dieser Eigenschaft ein sehr unterdurchschnittliches Ergebnis in Bezug zur Vergleichsgruppe auf.");
        wordings.add(wording);
        wording = new Wording();
        wording.setMinValue(1d/6);
        wording.setText("Der Nutzer weist in dieser Eigenschaft ein unterdurchschnittliches Ergebnis in Bezug zur Vergleichsgruppe auf.");
        wordings.add(wording);
        wording = new Wording();
        wording.setMinValue(2d/6);
        wording.setText("Der Nutzer weist in dieser Eigenschaft ein eher unterdurchschnittliches Ergebnis in Bezug zur Vergleichsgruppe auf.");
        wordings.add(wording);
        wording = new Wording();
        wording.setMinValue(3d/6);
        wording.setText("Der Nutzer weist in dieser Eigenschaft ein durchschnittliches Ergebnis in Bezug zur Vergleichsgruppe auf.");
        wordings.add(wording);
        wording = new Wording();
        wording.setMinValue(4d/6);
        wording.setText("Der Nutzer weist in dieser Eigenschaft ein eher überdurchschnittliches Ergebnis in Bezug zur Vergleichsgruppe auf.");
        wordings.add(wording);
        wording = new Wording();
        wording.setMinValue(5d/6);
        wording.setText("Der Nutzer weist in dieser Eigenschaft ein überdurchschnittliches Ergebnis in Bezug zur Vergleichsgruppe auf.");
        wordings.add(wording);
        wordings.forEach(w -> {
            w.setTestId(testId);
            w.setSkillId(skillId);
            w.setCustomerId(customerId);
        });
        return wordings;
    }

    private String formatValue(double value){
        String formatted = String.valueOf(value*100d);
        String[] spl = formatted.split("\\.");
        if(spl.length>1){
            formatted = spl[1];
            if(formatted.length()>2)
                formatted = formatted.substring(0, 2);
            if(formatted.length()<2)
                formatted+="0";
            formatted = spl[0]+"."+formatted;
        }else{
            formatted = spl[0]+".00";
        }
        return formatted;
    }

    public String getText(User candidate, double value){
        String text = getText();
        text = text.replace("{firstname}", candidate.getFirstName());
        text = text.replace("{lastname}", candidate.getLastName());
        text = text.replace("{title}", candidate.getGender().getTitle());
        text = text.replace("{fullname}", candidate.getFirstName()+" "+candidate.getLastName());
        text = text.replace("{value}", formatValue(value));
        return text;
    }

    public static Wording findDefaultWording(int testId, int skillId, int customerId, double value){
        List<Wording> defaults = defaultWordings(testId, skillId, customerId).stream().filter(wording -> wording.getMinValue() <= value).collect(Collectors.toList());
        if(defaults.size()==0)
            return null;
        Wording high = defaults.get(0);
        for(Wording wording : defaults)
            if(wording.getMinValue()>high.getMinValue())
                high = wording;
        return high;
    }

    public static Wording findWording(int testId, int skillId, int customerId, double value){
        Wording wording = Wording.getTable().builder().where().eq("test_id", testId).and().eq("skill_id", skillId).and().c(" `min_value`<='"+value+"'").and().eq("customer_id", customerId).order("min_value", true).limit(1).query().first();
        if(wording == null && customerId != 0){
            wording = Wording.getTable().builder().where().eq("test_id", testId).and().eq("skill_id", skillId).and().c(" `min_value`<='"+value+"'").and().eq("customer_id", 0).order("min_value", true).limit(1).query().first();
        }
        if(wording == null){
            wording = findDefaultWording(testId, skillId, customerId, value);
        }
        return wording;
    }

}
