package de.advertal.assest.models.web;

import com.google.gson.JsonArray;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SolvableResponse {
    boolean success;
    double progress;
    String test;
    Object data;
    public SolvableResponse(boolean success, double progress, String test){
        this.success = success;
        this.progress = progress;
        this.test = test;
        this.data = new JsonArray();
    }
    public SolvableResponse(boolean success, double progress){
        this(success, progress, "");
    }
}
