package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.List;

@DatabaseTable("skill_results")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SkillResult {

    @Getter @Setter
    static Table<SkillResult, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "test_run_id")
    int testRunId;
    @DatabaseField(columnName = "skill_id")
    int skillId;
    @DatabaseField
    ResultState state = ResultState.CREATED;
    @DatabaseField
    double value = 0;

    private transient Skill cachedSkill = null;

    public void calculate(){
        Skill skill = getSkill();
        List<Item> items = skill.getItems();
        double totalWeight = 0;
        value = 0;
        for(Item item : items)
            totalWeight += item.getWeight();
        for(Item item : items){
            Answer answer = Answer.getTable().builder().where().eq("test_run_id", getTestRunId()).and().eq("item_id", item.getId()).query().first();
            value += item.assertAnswer(answer.getValue())*(item.getWeight()/totalWeight);
        }
        if(value>0.999)
            value = 1;
        if(value<0.001)
            value = 0;
    }

    public double getValueInPercent(){
        return getValue()*100;
    }

    public String getValueInPercentString(){
        return ""+getValueInPercent();
    }

    public Skill getSkill(){
        if(cachedSkill == null)
            cachedSkill = Skill.getTable().queryById(getSkillId());
        return cachedSkill;
    }

    public TestRun getTestRun(){
        return TestRun.getTable().queryById(getTestRunId());
    }

    public Wording getWording(){
        TestRun run = getTestRun();
        return Wording.findWording(run.getTestId(), getSkillId(), run.getCustomerId(), getValue());
    }

}
