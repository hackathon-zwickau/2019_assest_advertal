package de.advertal.assest.models.web;

import de.advertal.assest.models.database.Skill;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EditSkillResponse {
    boolean success;
    String message;
    String name;
    String description;
    boolean soft;
    int category;
    public EditSkillResponse(boolean success, String message){
        this.success = success;
        this.message = message;
        this.name = "";
        this.description = "";
        this.soft = false;
        this.category = 0;
    }
    public EditSkillResponse(boolean success, String message, Skill skill){
        this.success = success;
        this.message = message;
        this.name = skill.getName();
        this.description = skill.getDescription();
        this.soft = skill.isSoft();
        this.category = skill.getCategoryId();
    }
}
