package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.List;

@DatabaseTable("skills")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Skill {

    @Getter @Setter
    static Table<Skill, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "customer_id")
    int customerId;
    @DatabaseField(columnName = "category_id")
    int categoryId;
    @DatabaseField
    String name;
    @DatabaseField
    String description;
    @DatabaseField
    boolean soft = false;
    int amount = 0;

    public void checkAmount(){
        this.amount = getItems().size();
    }

    public User getCustomer(){
        if(customerId < 1)
            return null;
        return User.getTable().queryById(customerId);
    }

    public boolean canRead(User user){
        return customerId == 0 || customerId == user.getId();
    }

    public boolean isShared(){
        return getCustomerId() < 1;
    }

    public Category getCategory(){
        return Category.getTable().queryById(categoryId);
    }

    public List<Item> getItems(){
        return Item.getTable().queryForEq("skill_id", getId());
    }

    public String getDisplayDescription(){
        if(getDescription().length()==0)
            return "Keine Beschreibung vorhanden";
        return getDescription();
    }

    public void delete(){
        Item.getTable().queryForEq("skill_id", getId()).forEach(item -> {
            Answer.getTable().builder().where().eq("item_id", item.getId()).delete();
            Item.getTable().delete(item);
        });
        SkillResult.getTable().builder().where().eq("skill_id", getId());
        List<TestSkill> testSkills = TestSkill.getTable().queryForEq("skill_id", getId());
        for(TestSkill testSkill : testSkills){
            TestSkill.getTable().delete(testSkill);
            TestResult.getTable().builder().where().eq("test_id", testSkill.getTestId()).delete();
            for(TestRun run : TestRun.getTable().queryForEq("test_id", testSkill.getTestId())){
                if(run.getState() == ResultState.DONE){
                    run.setState(ResultState.SUBMITTED);
                    TestRun.getTable().update(run);
                }else{
                    run.setCurrent(0);
                    run.setState(ResultState.CREATED);
                    TestRun.getTable().update(run);
                }
            }
        }
        getTable().delete(this);
    }

}
