package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.List;

@DatabaseTable("categories")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Category {

    @Getter @Setter
    static Table<Category, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "customer_id")
    int customerId;
    @DatabaseField(columnName = "parent_id")
    int parentId;
    @DatabaseField
    String name;

    public boolean isShared(){
        return getCustomerId() < 1;
    }

    public User getCustomer(){
        if(customerId < 1)
            return null;
        return User.getTable().queryById(customerId);
    }

    public boolean hasParent(){
        return getParentId() > 0;
    }

    public Category getParent(){
        if(parentId<1)
            return null;
        return Category.getTable().queryById(parentId);
    }

    public List<Skill> getSkills(){
        return Skill.getTable().queryForEq("category_id", getId());
    }

    public void delete(){
        getTable().queryForEq("parent_id", getId()).forEach(category -> {
            category.setParentId(0);
            getTable().update(category);
        });
        Skill.getTable().queryForEq("category_id", getId()).forEach(skill -> {
            skill.setCategoryId(0);
            Skill.getTable().update(skill);
        });
        getTable().delete(this);
    }

}
