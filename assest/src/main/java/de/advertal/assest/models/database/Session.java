package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import spark.Response;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;

@DatabaseTable("sessions")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Session {

    private static final char[] TOKEN_CHARSET = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
    private static final int TOKEN_LENGTH = 40;
    private static final long SOON = 60000;
    public static final String COOKIE_NAME = "ASSEST_SESSION";
    public static final int COOKIE_LIFETIME = 3600; //1 Hour

    @Getter @Setter
    static Table<Session, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField
    Type type;
    @DatabaseField(columnName = "user_id")
    int userId;
    @DatabaseField(columnName = "admin_id")
    int adminId;
    @DatabaseField
    String token;
    @DatabaseField(columnName = "keep_session")
    boolean keepSession;
    @DatabaseField(columnName = "delete_at")
    Timestamp deleteAt;

    public void refresh(){
        Date date = new Date();
        date.setTime(date.getTime() + (COOKIE_LIFETIME*1000));
        deleteAt = Timestamp.from(date.toInstant());
        System.out.println(deleteAt);
    }

    public void apply(Response response){
        response.cookie("/", COOKIE_NAME, getToken(), isKeepSession() ? COOKIE_LIFETIME : 0, false, false);
    }

    public boolean isExpired(){
        return deleteAt.getTime() < new Date().getTime();
    }

    public boolean willExpireSoon(){
        return deleteAt.getTime() - new Date().getTime() < SOON;
    }

    public boolean isSU(){
        return adminId > 0;
    }

    public void setSU(int userId){
        if(adminId < 1)
            adminId = this.userId;
        this.userId = userId;
    }

    public void returnSU(){
        if(adminId > 0){
            userId = adminId;
            adminId = 0;
        }
    }

    public void generateToken(){
        StringBuilder sb = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for(int i = 0; i < TOKEN_LENGTH; i++)
            sb.append(TOKEN_CHARSET[random.nextInt(TOKEN_CHARSET.length)]);
        this.token = sb.toString();
    }

    public User getUser() {
        return User.getTable().queryById(getUserId());
    }

    public enum Type {
        WEB
    }

}
