package de.advertal.assest.models.database;

import de.advertal.assest.Assest;
import de.advertal.assest.utils.RandomUtil;
import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@DatabaseTable("test_runs")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TestRun {

    @Getter @Setter
    static Table<TestRun, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "customer_id")
    int customerId;
    @DatabaseField(columnName = "test_id")
    int testId;
    @DatabaseField(columnName = "user_id")
    int userId;
    @DatabaseField
    ResultState state = ResultState.CREATED;
    @DatabaseField
    int current = 0;
    @DatabaseField
    String token;
    @DatabaseField
    String random;
    @DatabaseField(columnName = "created_at")
    Timestamp createdAt;
    @DatabaseField(columnName = "solved_at")
    Timestamp solvedAt;

    private transient Test cachedTest = null;

    public Test getTest(){
        if(cachedTest == null)
            cachedTest = Test.getTable().queryById(testId);
        return cachedTest;
    }

    public User getUser(){
        return User.getTable().queryById(userId);
    }

    public String getRandom(){
        return id +String.valueOf(testId)+ userId;
    }

    public List<SkillResult> getModuleResults(){
        return SkillResult.getTable().queryForEq("test_run_id", getId());
    }

    public TestResult getTestResult(){
        return TestResult.getTable().builder().where().eq("test_run_id", getId()).query().first();
    }

    public List<Skill> getSkills(){
        List<Skill> skills = new ArrayList<>();
        for(TestSkill relation : TestSkill.getTable().queryForEq("test_id", getTestId()))
            skills.add(relation.getSkill());
        return skills;
    }

    public List<Item> getItems(){
        List<Item> items = new ArrayList<>();
        List<Item> soft = new ArrayList<>();
        for(Skill skill : getSkills()){
            if(skill.isSoft()){
                soft.addAll(skill.getItems());
            }else{
                items.addAll(skill.getItems());
            }
        }
        soft = new ArrayList<>(RandomUtil.randomizeItems(soft, getRandom()));
        soft.addAll(items);
        return soft;
    }

    public List<Item> getItemsRandomized(){
        return getItems();
    }

    public void generateRandom(){
        char[] charSet = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<50;i++)
            sb.append(charSet[random.nextInt(charSet.length)]);
        this.random = sb.toString();
    }

    public void generateToken(){
        char[] charSet = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<20;i++)
            sb.append(charSet[random.nextInt(charSet.length)]);
        this.token = sb.toString();
    }

    public boolean canRead(User user){
        return user.getType().isAdmin() || customerId == user.getId();
    }

    public boolean canSolve(User user){
        return user.getType().isAdmin() || customerId == user.getId() || userId == user.getId();
    }

    public boolean canWrite(User user){
        return user.getType().isAdmin() || customerId == user.getId();
    }

    public void delete(){
        for(TestResult result : TestResult.getTable().queryForEq("test_run_id", getId()))
            TestResult.getTable().delete(result);
        for(SkillResult result : SkillResult.getTable().queryForEq("test_run_id", getId()))
            SkillResult.getTable().delete(result);
        for(Answer result : Answer.getTable().queryForEq("test_run_id", getId()))
            Answer.getTable().delete(result);
        getTable().delete(this);
    }

}
