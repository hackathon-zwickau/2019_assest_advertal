package de.advertal.assest.models.web;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import de.advertal.assest.models.database.Item;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class SolvableItem {

    Item.Type type;
    String question;
    String[] answers;
    Item.MediaType mediaType;
    String media;
    int timeLimit;

    public SolvableItem(Item.Type type, String question, String answers, Item.MediaType mediaType, String media, int timeLimit){
        this(type, question, parseAnswers(answers), mediaType, media, timeLimit);
    }

    public SolvableItem(Item item){
        this(item.getType(), item.getQuestion(), item.getAnswers(), item.getMediaType(), item.getMedia(), item.getTimeLimit());
    }

    private static String[] parseAnswers(String answers){
        String[] answersArray = new String[0];
        if(answers.length()>0){
            JsonArray jsonArray = new JsonParser().parse(answers).getAsJsonArray();
            answersArray = new String[jsonArray.size()];
            int i = 0;
            for(JsonElement jsonElement : jsonArray){
                answersArray[i] = jsonElement.getAsString();
                i++;
            }
        }
        return answersArray;
    }

}
