package de.advertal.assest.models.web;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import de.advertal.assest.models.database.Item;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EditItemResponse {
    boolean success;
    String message;
    Item.Type type;
    String question;
    boolean invert;
    String[] answers;
    String[] correct;
    Item.MediaType mediaType;
    String media;
    public EditItemResponse(boolean success, String message){
        this.success = success;
        this.message = message;
        this.question = "";
        this.invert = false;
        type = Item.Type.LIKERT4;
        answers = new String[0];
        correct = new String[0];
        mediaType = Item.MediaType.NONE;
        media = "";
    }
    public EditItemResponse(boolean success, String message, Item item){
        this.success = success;
        this.message = message;
        this.question = item.getQuestion();
        this.invert = item.isInvert();
        this.mediaType = item.getMediaType();
        this.media = item.getMedia();
        this.type = item.getType();
        JsonElement element = new JsonParser().parse(item.getAnswers());
        JsonArray answersArray = new JsonArray();
        if(!element.isJsonNull()){
            answersArray = element.getAsJsonArray();
        }
        answers = new String[answersArray.size()];
        for(int i=0;i<answers.length;i++)
            answers[i] = answersArray.get(i).getAsString();
        List<Integer> correctNumbers = new ArrayList<>();
        for(String s : item.getCorrectAnswer().split(","))
            if(s.length() > 0)
                correctNumbers.add(Integer.parseInt(s));
        correct = new String[correctNumbers.size()];
        for(int i=0; i<correctNumbers.size();i++)
            correct[i] = answers[correctNumbers.get(i)-1];
    }
}
