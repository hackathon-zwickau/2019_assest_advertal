package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@DatabaseTable("test_results")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TestResult {

    @Getter @Setter
    static Table<TestResult, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "test_run_id")
    int testRunId;
    @DatabaseField
    ResultState state = ResultState.CREATED;
    @DatabaseField
    double value;
    @DatabaseField
    boolean items = false;

    private transient TestRun cachedRun = null;

    public void calculate(){
        List<TestSkill> skills = getSkillRelations();
        double totalWeight = 0;
        value = 0;
        int items = 0;
        for(TestSkill skill : skills){
            if(skill.getSkill().isSoft())
                continue;
            totalWeight += skill.getWeight();
        }
        for(TestSkill skill : skills){
            if(skill.getSkill().isSoft())
                continue;
            SkillResult result = SkillResult.getTable().builder().where().eq("test_run_id", getTestRunId()).and().eq("skill_id", skill.getSkillId()).query().first();
            value += result.getValue()*(skill.getWeight()/totalWeight);
            items++;
        }
        this.items = items>0;
        if(value>0.999)
            value = 1;
        if(value<0.001)
            value = 0;
    }

    public List<Skill> getSkills(){
        List<Skill> skills = new ArrayList<>();
        for(TestSkill relation : getSkillRelations()){

            skills.add(relation.getSkill());
        }
        return skills;
    }

    public double getValueInPercent(){
        return getValue()*100;
    }

    public String getValueInPercentString(){
        return ""+getValueInPercent();
    }

    public List<TestSkill> getSkillRelations(){
        return TestSkill.getTable().queryForEq("test_id", TestRun.getTable().queryById(getTestRunId()).getTestId());
    }

    public TestRun getTestRun(){
        if(cachedRun == null)
            cachedRun = TestRun.getTable().queryById(getTestRunId());
        return cachedRun;
    }

    public Wording getWording(){
        TestRun run = getTestRun();
        return Wording.findWording(run.getTestId(), 0, run.getCustomerId(), getValue());
    }

}
