package de.advertal.assest.models.web;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
public class SkillCreateRequest {

    String name;
    String description;
    boolean soft;
    int categoryId;

}
