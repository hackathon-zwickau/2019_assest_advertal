package de.advertal.assest.models.web;

import de.advertal.assest.models.database.Item;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class UpdateItemRequest {

    Item.Type type;
    String question;
    Item.MediaType mediaType;
    String media;
    String answers;
    String correct;
    boolean invert;

}
