package de.advertal.assest.models.web;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class UpdateSkillRequest {

    String name;
    String description;
    boolean soft;
    int category;

}
