package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@DatabaseTable("test_skills")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TestSkill {

    @Getter @Setter
    static Table<TestSkill, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "test_id")
    int testId;
    @DatabaseField(columnName = "skill_id")
    int skillId;
    @DatabaseField
    double weight;

    public Test getTest(){
        return Test.getTable().queryById(getTestId());
    }

    public Skill getSkill(){
        return Skill.getTable().queryById(getSkillId());
    }

}
