package de.advertal.assest.models.web;

import de.advertal.assest.models.database.User;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateUserRequest {

    String firstName;
    String lastName;
    String email;
    User.Gender gender;
    String password;

}
