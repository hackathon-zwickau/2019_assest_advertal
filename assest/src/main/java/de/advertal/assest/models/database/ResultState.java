package de.advertal.assest.models.database;

public enum ResultState {

    CREATED,
    SUBMITTED,
    PENDING,
    FAILED,
    DONE

}
