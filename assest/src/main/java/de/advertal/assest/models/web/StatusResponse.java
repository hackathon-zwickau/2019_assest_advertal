package de.advertal.assest.models.web;

import com.google.gson.JsonArray;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StatusResponse {
    boolean success;
    String message;
    Object data;
    public StatusResponse(boolean success, String message){
        this.success = success;
        this.message = message;
        this.data = new JsonArray();
    }
}
