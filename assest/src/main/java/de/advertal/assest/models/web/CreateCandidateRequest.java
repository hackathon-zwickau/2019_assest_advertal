package de.advertal.assest.models.web;

import de.advertal.assest.models.database.User;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateCandidateRequest {

    String firstName;
    String lastName;
    String email;
    User.Gender gender;
    int customer;

}
