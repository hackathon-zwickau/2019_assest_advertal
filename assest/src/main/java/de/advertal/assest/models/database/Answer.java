package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@DatabaseTable("answers")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Answer {

    @Getter @Setter
    static Table<Answer, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "test_run_id")
    int testRunId;
    @DatabaseField(columnName = "item_id")
    int itemId;
    @DatabaseField
    String value;

}
