package de.advertal.assest.models.web;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Base64;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class ImportRequest {

    int customerId;
    ImportType type;
    DataType dataType;
    String data;

    public enum ImportType {
        ITEMS,
        USERS
    }

    public enum DataType {
        CSV1
    }

    public String decodeString(){
        return new String(Base64.getDecoder().decode(data.split(",")[1]));
    }

}
