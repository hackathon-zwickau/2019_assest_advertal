package de.advertal.assest.models.web;

import de.advertal.assest.models.database.Item;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
public class ItemCreateRequest {

    String question;
    Item.Type type;
    int skill;
    boolean invert;
    String answers;
    String correct;
    Item.MediaType mediaType;
    String media;

}
