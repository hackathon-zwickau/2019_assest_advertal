package de.advertal.assest.models.web;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestCreationResponse {
    boolean success;
    String message;
    int test;
    public TestCreationResponse(boolean success, String message){
        this(success, message, 0);
    }
}
