package de.advertal.assest.models.database;

import de.advertal.assest.utils.BCrypt;
import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Timestamp;

@DatabaseTable("users")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    @Getter @Setter
    static Table<User, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "parent_id")
    int parentId;
    @DatabaseField
    Type type;
    @DatabaseField
    String email;
    @DatabaseField(columnName = "first_name")
    String firstName;
    @DatabaseField(columnName = "last_name")
    String lastName;
    @DatabaseField
    Gender gender;
    @DatabaseField
    String password;
    @DatabaseField(columnName = "created_at")
    Timestamp createdAt;

    public void setPassword(String password){
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public boolean checkPassword(String password){
        return BCrypt.checkpw(password, this.password);
    }

    public User getParent(){
        if(parentId == 0)
            return null;
        return User.getTable().queryById(parentId);
    }

    public void deleteCandidate(){
        for(Session session : Session.getTable().queryForEq("user_id", getId())){
            if(session.isSU()) {
                session.returnSU();
            }else{
                Session.getTable().delete(session);
            }
        }
        for(TestRun run : TestRun.getTable().queryForEq("user_id", getId())){
            run.delete();
        }
        getTable().delete(this);
    }

    public void deleteCustomer(){
        for(Session session : Session.getTable().queryForEq("user_id", getId())){
            if(session.isSU()) {
                session.returnSU();
            }else{
                Session.getTable().delete(session);
            }
        }
        for(User user : User.getTable().queryForEq("parent_id", getId())){
            user.deleteCandidate();
        }
        for(Test test : Test.getTable().queryForEq("customer_id", getId())){
            test.delete();
        }
        getTable().delete(this);
    }

    @AllArgsConstructor
    @Getter
    public enum Gender {
        MALE("Herr"),
        FEMALE("Frau");
        private String title;
    }

    public enum Type {
        ADMIN,
        CUSTOMER,
        USER;
        public boolean isAdmin(){
            return this == ADMIN;
        }
        public boolean isCustomer(){
            return this == CUSTOMER;
        }
        public boolean isUser(){
            return this == USER;
        }
    }

}
