package de.advertal.assest.models.database;

import eu.bebendorf.ajorm.DatabaseField;
import eu.bebendorf.ajorm.DatabaseTable;
import eu.bebendorf.ajorm.Table;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@DatabaseTable("tests")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class Test implements Accessable {

    @Getter
    @Setter
    static Table<Test, Integer> table;

    @DatabaseField(id = true, ai = true, primary = true)
    int id;
    @DatabaseField(columnName = "customer_id")
    int customerId;
    @DatabaseField
    String name;
    @DatabaseField
    String description;
    @DatabaseField(columnName = "created_at")
    Timestamp createdAt;

    public String getUrl(){
        return "/tests/"+getId();
    }

    public boolean canRead(User user){
        return customerId == 0 || user.getType().isAdmin() || customerId == user.getId();
    }

    public boolean canWrite(User user){
        return user.getType().isAdmin() || customerId == user.getId();
    }

    public void delete(){
        for(TestRun run : TestRun.getTable().queryForEq("test_id", getId()))
            run.delete();
        for(TestSkill skill : TestSkill.getTable().queryForEq("test_id", getId()))
            TestSkill.getTable().delete(skill);
        Wording.getTable().builder().where().eq("test_id", getId()).delete();
        getTable().delete(this);
    }

    public String getDisplayDescription(){
        if(getDescription().length()==0)
            return "Keine Beschreibung vorhanden";
        return getDescription();
    }

    public List<Skill> getSkills(){
        List<Skill> skills = new ArrayList<>();
        TestSkill.getTable().queryForEq("test_id", getId()).forEach(rel -> skills.add(rel.getSkill()));
        return skills;
    }

}
