package de.advertal.assest;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class Branding {

    String name;
    String shortName;
    String imprintUrl;
    String privacyUrl;

}
