package de.advertal.assest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.bebendorf.ajorm.SQL;
import eu.bebendorf.ajorm.wrapper.MySQL;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class MySQLConfig {

    String host = "localhost";
    int port = 3306;
    String database = "assest";
    String username = "assest";
    String password = "changeme";

    public static MySQLConfig get(){
        if(new File("debug.mysql.json").exists())
            return get(true);
        return get(false);
    }

    public static MySQLConfig get(boolean debug){
        if(debug){
            File file = new File("debug.mysql.json");
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            if(!file.exists()){
                MySQLConfig config = new MySQLConfig();
                String json = gson.toJson(config);
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(json.getBytes());
                    fos.flush();
                    fos.close();
                }catch(IOException ex){

                }
                return config;
            }else{
                try {
                    FileInputStream fis = new FileInputStream(file);
                    byte[] data = new byte[fis.available()];
                    fis.read(data);
                    fis.close();
                    return gson.fromJson(new String(data), MySQLConfig.class);
                }catch(IOException ex){
                    return new MySQLConfig();
                }
            }
        }else{
            MySQLConfig config = new MySQLConfig();
            config.host = System.getenv("MYSQL_HOST");
            config.port = Integer.parseInt(System.getenv("MYSQL_PORT"));
            config.database = System.getenv("MYSQL_DATABASE");
            config.username = System.getenv("MYSQL_USERNAME");
            config.password = System.getenv("MYSQL_PASSWORD");
            return config;
        }
    }

    public SQL getSQL(){
        return new MySQL(host, port, database, username, password);
    }

}
