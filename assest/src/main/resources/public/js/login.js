$('#login-form').submit(function(event){
    let body = {
        email: $('#login-email').val(),
        password: $('#login-password').val(),
        keep_login: true
    };
    $.ajax({
        type: "POST",
        url: "/login",
        data: JSON.stringify(body),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function( data ) {
        alertSuccess(data,function(){
            if(data.success){
                window.location = "/";
            }else{
                $('#login-password').val("");
            }
        });
    });
    event.preventDefault();
});