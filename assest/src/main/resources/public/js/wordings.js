class Wordings {
    constructor(test, skill, wordings){
        this.test = test;
        this.skill = skill;
        this.wordings = wordings;
        this.elements = {
            displays: document.getElementById("wordings-displays"),
            distribution: document.getElementById("wordings-distribution")
        };
        if(this.wordings.length > 1){
            this.elements.distribution.value = (this.wordings.length/2)*this.wordings[1].minValue*100;
        }
    }

    display(){
        while (this.elements.displays.firstChild) {
            this.elements.displays.removeChild(this.elements.displays.firstChild);
        }
        this.wordings.forEach(wording => {
            this.elements.displays.appendChild(this.generateDisplay(wording));
        });
        this.elements.displays.appendChild(this.generatePlusDisplay());
    }
    parse(){
        var childs = this.elements.displays.childNodes;
        this.wordings = [];
        for(var i=0; i < childs.length-1; i++){
            this.wordings.push(this.parseCol(childs[i]));
        }
    }
    parseCol(col){
        var panelBody = col.childNodes[0].childNodes[0];
        var valueInput = panelBody.childNodes[0].childNodes[1].childNodes[1];
        var textarea = panelBody.childNodes[1].childNodes[1];
        return {
            minValue: (valueInput.value / 100),
            text: textarea.value
        };
    }
    addWording(){
        this.elements.displays.removeChild(this.elements.displays.lastChild);
        this.elements.displays.appendChild(this.generateDisplay({
            minValue: 1,
            text: ""
        }));
        this.elements.displays.appendChild(this.generatePlusDisplay());
    }
    calcByDistribution(){
        var middle = this.elements.distribution.value/100;
        this.parse();
        var dist = this.calcDistributions(middle, this.wordings.length);
        for(var i = 0; i < dist.length; i++){
            this.wordings[i].minValue = dist[i];
        }
        this.display();
    }
    calcDistributions(middle, count){
        var leftStep = middle*2*(1/count);
        var rightStep = (1-middle)*2*(1/count);
        var even = count % 2 == 0;
        var sidesteps = count/2;
        if(!even){
            sidesteps -= 0.5;
        }
        var result = [];
        result[0] = 0;
        for(var i=1; i<=sidesteps; i++){
            result[i] = result[i-1] + leftStep;
        }
        var aStep = 0;
        if(!even){
            aStep = 1;
            result[sidesteps+aStep] = result[sidesteps] + ((leftStep+rightStep)/2);
        }
        for(var i=sidesteps+aStep+1; i < count; i++){
            result[i] = result[i-1] + rightStep;
        }
        return result;
    }
    generateDisplay(wording){
        var col = document.createElement("div");
        col.className = "col-md-3";
        var panel = document.createElement("div");
        panel.className = "panel panel-default panel-wording";
        panel.style.height = "300px";
        var panelBody = document.createElement("div");
        panelBody.className = "panel-body";
        var fg1 = document.createElement("div");
        fg1.className = "form-group";
        var lb1 = document.createElement("label")
        lb1.appendChild(document.createTextNode("Mindestwert"));
        var ig = document.createElement("div");
        ig.className = "input-group";
        var addon = document.createElement("div");
        addon.className = "input-group-addon";
        addon.appendChild(document.createTextNode("%"));
        ig.appendChild(addon);
        var valueInput = document.createElement("input");
        valueInput.className = "form-control";
        valueInput.type = "number";
        valueInput.min = 1;
        valueInput.max = 100;
        valueInput.step = 0.01;
        valueInput.value = (wording.minValue * 100).toFixed(2);
        if(wording.minValue == 0){
            valueInput.setAttribute("readonly","");
        }
        ig.appendChild(valueInput);
        fg1.appendChild(lb1);
        fg1.appendChild(ig);
        var fg2 = document.createElement("div");
        fg2.className = "form-group";
        var lb2 = document.createElement("label")
        lb2.appendChild(document.createTextNode("Text"));
        var textarea = document.createElement("textarea");
        textarea.className = "form-control";
        textarea.rows = 5;
        if(wording.minValue == 0){
            textarea.rows = 7;
        }
        textarea.appendChild(document.createTextNode(wording.text));
        fg2.appendChild(lb2);
        fg2.appendChild(textarea);
        panelBody.appendChild(fg1);
        panelBody.appendChild(fg2);
        if(wording.minValue != 0){
            var fg3 = document.createElement("div");
            fg3.className = "form-group";
            var button = document.createElement("button");
            button.className = "btn btn-danger btn-block";
            button.appendChild(document.createTextNode("Entfernen"));
            button.onclick = () => {
                this.elements.displays.removeChild(col);
            };
            fg3.appendChild(button);
            panelBody.appendChild(fg3);
        }
        panel.appendChild(panelBody);
        col.appendChild(panel);
        return col;
    }
    generatePlusDisplay(){
        var col = document.createElement("div");
        col.className = "col-md-3";
        var panel = document.createElement("div");
        panel.className = "panel panel-default";
        panel.style.height = "300px";
        var panelBody = document.createElement("div");
        panelBody.className = "panel-body text-center";
        var icon = document.createElement("i");
        icon.onclick = () => {
            this.addWording();
        };
        icon.className = "fas fa-plus-circle fa-5x";
        icon.style.marginTop = "70px";
        panelBody.appendChild(icon);
        panel.appendChild(panelBody);
        col.appendChild(panel);
        return col;
    }
    save(){
        this.parse();
        var url = "/api/wordings/"+this.test;
        if(this.skill > 0){
            url += "/"+this.skill;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(this.wordings),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function(data) {
            alertSuccess(data);
        });
    }
}