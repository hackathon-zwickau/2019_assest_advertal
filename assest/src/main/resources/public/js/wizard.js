class Wizard {
    constructor(){
        this.step = 1;
        this.selection = new SkillSelection();
        this.validation = new WizardValidation();
        this.selection.setCategory(-1);
        this.elements = {
            prev_button: $("#wizard-prev-button"),
            indicator1: document.getElementById("wizard-indicator-step-1"),
            indicator2: document.getElementById("wizard-indicator-step-2"),
            indicator3: document.getElementById("wizard-indicator-step-3"),
            display1: $("#wizard-display-step-1"),
            display2: $("#wizard-display-step-2"),
            display3: $("#wizard-display-step-3"),
            name: $("#wizard-base-name"),
            description: $("#wizard-base-description")
        };
    }
    setStep(step){
        this.step = step;
        if(step < 2){
            this.elements.prev_button.hide();
        }else{
            this.elements.prev_button.show();
        }
        this.elements.indicator1.className = "disabled";
        this.elements.indicator2.className = "disabled";
        this.elements.indicator3.className = "disabled";
        this.elements.display1.hide();
        this.elements.display2.hide();
        this.elements.display3.hide();
        switch(step){
            case 1:
                this.elements.indicator1.className = "selected";
                this.elements.display1.show();
                break;
            case 2:
                this.elements.indicator2.className = "selected";
                this.elements.display2.show();
                break;
            case 3:
                this.elements.indicator3.className = "selected";
                this.validation.update(this);
                this.elements.display3.show();
                break;
        }
        if(step == 2){
            $(".nano").nanoScroller({ alwaysVisible: true });
            $(".nano-pane").css("display","relative");
            $(".nano-slider").css("display","relative");
            setTimeout(() => {
                $(".nano").nanoScroller({ alwaysVisible: true });
                $(".nano-pane").css("display","relative");
                $(".nano-slider").css("display","relative");
            },500);
        }
    }
    next(){
        if(this.step<3){
            this.setStep(this.step+1);
        }else{
            $.ajax({
                type: "POST",
                url: "/api/tests",
                data: JSON.stringify(this.validation.data),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function(data) {
                alertSuccess(data,function(){
                    if(data.success){
                        window.location = "/tests/"+data.test;
                    }
                });
            });
        }
    }
    prev(){
        if(this.step>1){
            this.setStep(this.step-1);
        }
    }
}

class SkillSelection {
    constructor() {
        this.selectedSkills = [];
        this.allSkills = [];
        this.availableSkills = [];
        this.selectedCategory = 0;
        this.elements = {
            area_available: document.getElementById("wizard-area-skills-available"),
            area_selected: document.getElementById("wizard-area-skills-selected")
        };
    }
    selectSkill(id){
        this.selectedSkills.push(id);
        this.updateSelected();
        this.updateAvailable();
    }
    deselectSkill(id){
        this.selectedSkills = this.selectedSkills.filter(function(value, index, arr){
            return value != id;
        });
        this.updateSelected();
        this.updateAvailable();
    }
    setCategory(category){
        this.selectedCategory = category;
        this.updateAvailable(true);
        $.ajax({
            type: "GET",
            url: "/api/skills/category/"+category,
            dataType: "json"
        }).done((data) => {
            if(category == -1){
                this.allSkills = data;
            }
            this.availableSkills = data;
            this.updateAvailable();
        });
    }
    getSkill(id){
        for(var i=0; i < this.allSkills.length; i++){
            if(this.allSkills[i].id == id){
                return this.allSkills[i];
            }
        }
    }
    updateAvailable(loader = false){
        while (this.elements.area_available.firstChild) {
            this.elements.area_available.removeChild(this.elements.area_available.firstChild);
        }
        if(loader){
            this.elements.area_available.innerHTML = "Lädt...";
            return;
        }
        this.availableSkills.forEach(skill => {
            if(this.selectedSkills.includes(skill.id)){
                return;
            }
            var panel = document.createElement("div");
            panel.className = "panel panel-default panel-skill";
            var panelBody = document.createElement("div");
            panelBody.className = "panel-body panel-body-skill";
            panelBody.innerHTML = "<b>"+skill.name+"</b><br>Fragen: "+skill.amount;
            var selectButton = document.createElement("button");
            selectButton.onclick = () => {
                this.selectSkill(skill.id);
            };
            selectButton.className = "btn btn-primary btn-xs btn-skill-add";
            selectButton.innerHTML = "→";
            panelBody.appendChild(selectButton);
            panel.appendChild(panelBody);
            this.elements.area_available.appendChild(panel);
        });
        $(".nano").nanoScroller({ alwaysVisible: true });
        $(".nano-pane").css("display","relative");
        $(".nano-slider").css("display","relative");
    }
    updateSelected(){
        while (this.elements.area_selected.firstChild) {
            this.elements.area_selected.removeChild(this.elements.area_selected.firstChild);
        }
        this.selectedSkills.forEach(skillId => {
            var skill = this.getSkill(skillId);
            var skillItem = document.createElement("span");
            var deselectButton = document.createElement("span");
            deselectButton.onclick = () => {
                 this.deselectSkill(skill.id);
            };
            deselectButton.innerHTML = '<i class="fas fa-minus-square" style="color: red;"></i>';
            var skillText = document.createElement("span");
            skillText.innerHTML = " " + skill.name;
            skillItem.appendChild(deselectButton);
            skillItem.appendChild(skillText);
            this.elements.area_selected.appendChild(skillItem);
            this.elements.area_selected.appendChild(document.createElement("br"));
        });
    }
}

class WizardValidation {
    constructor() {
        this.elements = {
            name: document.getElementById("wizard-validation-name"),
            description: document.getElementById("wizard-validation-description"),
            items: document.getElementById("wizard-validation-items"),
            skills: document.getElementById("wizard-validation-skills")
        };
        this.data = {
            name: "",
            description: "",
            skills: []
        };
    }
    update(wizard){
        this.data.name = wizard.elements.name.val();
        this.data.description = wizard.elements.description.val();
        this.data.skills = wizard.selection.selectedSkills;
        var itemCount = 0;
        while (this.elements.skills.firstChild) {
            this.elements.skills.removeChild(this.elements.skills.firstChild);
        }
        this.data.skills.forEach(skillId => {
            var skill = wizard.selection.getSkill(skillId);
            itemCount += skill.amount;
            this.elements.skills.appendChild(document.createTextNode(skill.name));
            this.elements.skills.appendChild(document.createElement("br"));
        });
        this.elements.name.innerHTML = this.data.name;
        this.elements.description.innerHTML = this.data.description;
        this.elements.items.innerHTML = itemCount;
    }
}

var wizard = new Wizard();