function copyToClipboard(value) {
    let target = document.getElementById("clipboard-fake");
    target.value = value;
    let currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    document.execCommand("copy");
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
}
function alertSuccess(response,after = function(){}){
    swal({
        title: response.success?"Erfolgreich!":"Fehler!",
        text: response.message,
        buttons: false,
        timer: 2000,
        icon: response.success?"success":"error"
    }).then(after);
}
function logout(){
    $.ajax({
        type: "POST",
        url: "/logout",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function( data ) {
        alertSuccess(data,function(){
            if(data.success){
                window.location = "/login";
            }
        });
    });
}
function changePassword(){
    $.ajax({
        type: "POST",
        url: "/password",
        data: JSON.stringify({
            password: $("#change-password-password").val(),
            repeat: $("#change-password-repeat").val()
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function( data ) {
        $("#changePasswordModal").modal("hide");
        alertSuccess(data, function(){
            if(data.success){
                window.location = "/login";
            }
        });
    });
    $("#change-password-password").val("");
    $("#change-password-repeat").val("");
}