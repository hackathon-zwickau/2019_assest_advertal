<#macro page_body>
    <div class="x_panel">
        <div class="x_title">
            <h2>Inhalte <small>Kategorien</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a><button class="btn btn-primary" data-toggle="modal" data-target="#createCategoryModal">Neue Kategorie</button></a></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="categoriesTable" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Oberkategorie</th>
                        <th>Aktionen</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="createCategoryModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createCategoryModalLabel">Kategorie erstellen</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" id="create-category-name">
                    </div>
                    <div class="form-group">
                        <label>Oberkategorie</label>
                        <select class="form-control" id="create-category-parent">
                            <option value="0">Keine</option>
                            <#list categories as category>
                            <option value="${ category.id }">${ category.name }</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" onclick="createCategory()">Erstellen</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function deleteCategory(id){
            $.ajax({
                type: "DELETE",
                url: "/api/categories/"+id,
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        window.location.reload();
                    }
                });
            });
        }
        function createCategory(){
            var name = $('#create-category-name').val();
            var parentId = $('#create-category-parent').val();
            let body = {
                name: name,
                parentId: parentId
            };
            $.ajax({
                type: "POST",
                url: "/api/categories",
                data: JSON.stringify(body),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function( data ) {
                $('#createCategoryModal').modal('hide');
                alertSuccess(data,function(){
                    if(data.success){
                        window.location.reload();
                    }
                });
            });
        }
        var categoriesTable;
        $(document).ready( function () {
            categoriesTable = $('#categoriesTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
                },
                ajax: "/table/categories/all"
            });
        });
    </script>
</#macro>
<#include "../base.ftl" />