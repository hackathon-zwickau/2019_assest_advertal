<#macro page_body>
    <style>
        .panel-wording {
            background-color: #E5E5E5;
        }
    </style>
    <a href="<#if !skills??>/wordings/${ testId }<#else>/tests/${ testId }</#if>" class="btn btn-default">Zurück</a>
    <div class="x_panel">
        <div class="x_title">
            <h2>Auswertungstexte <small>${ wordingtitle }</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group text-center">
                        <label>Mittelwert</label>
                        <div class="input-group">
                            <div class="input-group-addon">%</div>
                            <input type="number" min="1" max="99" step="1" value="50" class="form-control" id="wordings-distribution">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-primary" onclick="wordings.calcByDistribution()">Berechnen</button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" id="wordings-displays"></div>
            <hr>
            <div class="text-center">
                <button class="btn btn-primary" onclick="wordings.save()">Speichern</button>
            </div>
        </div>
    </div>
    <#if skills??>
    <div class="x_panel">
        <div class="x_title">
            <h2>Skills</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table">
                <#list skills as skill>
                <tr><td>${ skill.name }</td><td><a href="/wordings/${ testId }/${ skill.id }" class="btn btn-info btn-xs">Bearbeiten</a></td></tr>
                </#list>
            </table>
        </div>
    </div>
    </#if>
    <script src="/js/wordings.js"></script>
    <script>
        var wordings = new Wordings(${ testId }, ${ skillId }, ${ wordings });
        wordings.display();
    </script>
</#macro>
<#include "base.ftl" />