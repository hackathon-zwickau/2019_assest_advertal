<#macro page_body>
    <div class="x_panel">
        <div class="x_title">
            <h2>Benutzerverwaltung <small>Bewerber</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a><button class="btn btn-primary" data-toggle="modal" data-target="#createUserModal">Neuer Bewerber</button></a></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="candidatesTable" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                    <tr>
                        <th>Name</th>
                        <#if user.type.isAdmin()>
                        <th>Bewerber von</th>
                        </#if>
                        <th>Aktionen</th>
                        <th>Registriert am</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="createUserModal" tabindex="-1" role="dialog" aria-labelledby="createUserModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createUserModalLabel">Bewerber erstellen</h4>
                </div>
                <div class="modal-body">
                    <#if user.getType().isAdmin()>
                    <div class="form-group">
                        <label>Kunde auswählen</label>
                        <select class="form-control" id="create-user-customer">
                            <#list customers as customer>
                            <option value="${ customer.id }">${ customer.firstName } ${ customer.lastName }</option>
                            </#list>
                        </select>
                    </div>
                    </#if>
                    <div class="form-group">
                        <label>Ansprache</label>
                        <select class="form-control" id="create-user-gender">
                            <option value="MALE">Herr</option>
                            <option value="FEMALE">Frau</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Vorname</label>
                        <input class="form-control" id="create-user-firstname">
                    </div>
                    <div class="form-group">
                        <label>Nachname</label>
                        <input class="form-control" id="create-user-lastname">
                    </div>
                    <div class="form-group">
                        <label>E-Mail Adresse</label>
                        <input class="form-control" id="create-user-email">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" onclick="createUser()">Erstellen</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function deleteUser(id){
            $.ajax({
                type: "DELETE",
                url: "/api/candidates/"+id,
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        candidatesTable.ajax.reload();
                    }
                });
            });
        }
        function createUser(){
            var firstName = $('#create-user-firstname').val();
            var lastName = $('#create-user-lastname').val();
            var email = $('#create-user-email').val();
            var gender = $('#create-user-gender').val();
            let body = {
                firstName: firstName,
                lastName: lastName,
                email: email,
                gender: gender
            };
            <#if user.type.isAdmin()>
            body.customer = parseInt($('#create-user-customer').val());
            </#if>
            $.ajax({
                type: "POST",
                url: "/api/candidates",
                data: JSON.stringify(body),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function( data ) {
                $('#createUserModal').modal('hide');
                alertSuccess(data,function(){
                    if(data.success){
                        candidatesTable.ajax.reload();
                    }
                });
            });
        }
        var candidatesTable;
        $(document).ready( function () {
            candidatesTable = $('#candidatesTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
                },
                ajax: "/table/candidates/all"
            });
        });
    </script>
</#macro>
<#include "base.ftl" />