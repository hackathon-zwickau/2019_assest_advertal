<#macro page_body>
    <style>
        .scale-soft {
            background: rgba(199,199,199,1);
            background: -moz-linear-gradient(left, rgba(199,199,199,1) 0%, rgba(117,116,117,1) 31%, rgba(82,82,82,1) 47%, rgba(82,82,82,1) 51%, rgba(82,82,82,1) 55%, rgba(110,110,110,1) 72%, rgba(199,199,199,1) 100%);
            background: -webkit-gradient(left top, right top, color-stop(0%, rgba(199,199,199,1)), color-stop(31%, rgba(117,116,117,1)), color-stop(47%, rgba(82,82,82,1)), color-stop(51%, rgba(82,82,82,1)), color-stop(55%, rgba(82,82,82,1)), color-stop(72%, rgba(110,110,110,1)), color-stop(100%, rgba(199,199,199,1)));
            background: -webkit-linear-gradient(left, rgba(199,199,199,1) 0%, rgba(117,116,117,1) 31%, rgba(82,82,82,1) 47%, rgba(82,82,82,1) 51%, rgba(82,82,82,1) 55%, rgba(110,110,110,1) 72%, rgba(199,199,199,1) 100%);
            background: -o-linear-gradient(left, rgba(199,199,199,1) 0%, rgba(117,116,117,1) 31%, rgba(82,82,82,1) 47%, rgba(82,82,82,1) 51%, rgba(82,82,82,1) 55%, rgba(110,110,110,1) 72%, rgba(199,199,199,1) 100%);
            background: -ms-linear-gradient(left, rgba(199,199,199,1) 0%, rgba(117,116,117,1) 31%, rgba(82,82,82,1) 47%, rgba(82,82,82,1) 51%, rgba(82,82,82,1) 55%, rgba(110,110,110,1) 72%, rgba(199,199,199,1) 100%);
            background: linear-gradient(to right, rgba(199,199,199,1) 0%, rgba(117,116,117,1) 31%, rgba(82,82,82,1) 47%, rgba(82,82,82,1) 51%, rgba(82,82,82,1) 55%, rgba(110,110,110,1) 72%, rgba(199,199,199,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c7c7c7', endColorstr='#c7c7c7', GradientType=1 );
            height: 16px;
            width: 100%;
            position: relative;
        }
        .scale {
            background: rgba(61,59,61,1);
            background: -moz-linear-gradient(left, rgba(61,59,61,1) 0%, rgba(89,89,89,1) 24%, rgba(150,150,150,1) 59%, rgba(240,240,240,1) 100%);
            background: -webkit-gradient(left top, right top, color-stop(0%, rgba(61,59,61,1)), color-stop(24%, rgba(89,89,89,1)), color-stop(59%, rgba(150,150,150,1)), color-stop(100%, rgba(240,240,240,1)));
            background: -webkit-linear-gradient(left, rgba(61,59,61,1) 0%, rgba(89,89,89,1) 24%, rgba(150,150,150,1) 59%, rgba(240,240,240,1) 100%);
            background: -o-linear-gradient(left, rgba(61,59,61,1) 0%, rgba(89,89,89,1) 24%, rgba(150,150,150,1) 59%, rgba(240,240,240,1) 100%);
            background: -ms-linear-gradient(left, rgba(61,59,61,1) 0%, rgba(89,89,89,1) 24%, rgba(150,150,150,1) 59%, rgba(240,240,240,1) 100%);
            background: linear-gradient(to right, rgba(61,59,61,1) 0%, rgba(89,89,89,1) 24%, rgba(150,150,150,1) 59%, rgba(240,240,240,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3d3b3d', endColorstr='#f0f0f0', GradientType=1 );
            height: 16px;
            width: 100%;
            position: relative;
        }
        .scale-indicator {
            background-color: #00ace6;
            height: 40px;
            width: 5px;
            top: -28px;
            position: relative;
            border: 1px solid white;
        }
        h3 {
            margin-bottom: 25px;
        }
    </style>
    <div class="x_panel">
        <div class="x_title">
            <h2>Auswertung <small>${ test.name }</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <#if testValue??>
            <h3 class="text-center">Gesamt</h3>
            <div style="width: 100%;">
                <div class="scale"></div>
                <div class="scale-indicator" style="left: ${ testValue }%;"></div>
            </div>
            <h4>Beschreibung</h4>
            ${ test.displayDescription }
            <h4>Auswertung</h4>
            ${ testwording.text }
            </#if>
            <#list skillresults as skillresult>
            <hr>
            <h3 class="text-center">${ skillresult.skill.name }</h3>
            <div style="width: 100%;">
                <div class="scale<#if skillresult.skill.isSoft()>-soft</#if>"></div>
                <div class="scale-indicator" style="left: ${ skillresult.getValueInPercentString() }%;"></div>
            </div>
            <h4>Beschreibung</h4>
            ${ skillresult.skill.displayDescription }
            <h4>Auswertung</h4>
            Hier wird später das Wording für den erreichten Wert stehen.
            </#list>
        </div>
    </div>
</#macro>
<#include "base.ftl" />