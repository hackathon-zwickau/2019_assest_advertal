<#macro page_body>
    <div class="x_panel">
        <div class="x_title">
            <h2>Alle Tests <small>Ihre Tests</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a href="/wizard"><button class="btn btn-primary">Test erstellen</button></a></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="testsTable" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                    <tr>
                        <th>Test</th>
                        <th><#if user.getType().isAdmin()>Kunde<#else>Typ</#if></th>
                        <th>Aktionen</th>
                        <th>Erstellt am</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        function deleteTest(id){
            $.ajax({
                type: "DELETE",
                url: "/api/tests/"+id,
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        testsTable.ajax.reload();
                    }
                });
            });
        }
        var testsTable;
        $(document).ready( function () {
            testsTable = $('#testsTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
                },
                ajax: "/table/tests/all"
            });
        });
    </script>
</#macro>
<#include "base.ftl" />