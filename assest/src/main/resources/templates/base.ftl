<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>${ branding.name }</title>

    <!-- Bootstrap -->
    <link href="${cdn}/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- NProgress -->
    <link href="${cdn}/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="${cdn}/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="${cdn}/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="${cdn}/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="${cdn}/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <link href="${cdn}/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="${cdn}/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="${cdn}/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="${cdn}/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="${cdn}/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="${cdn}/build/css/custom.min.css" rel="stylesheet">

    <link rel="stylesheet" href="/css/nanoscroller.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/js/helper.js"></script>

    <style>
    #clipboard-fake {
        opacity: 0;
        width: 5px;
    }
    </style>

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><span>${ branding.name }</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_info">
                        <span>Herzlich Wilkommen</span>
                        <h2>${ user.gender.title } ${ user.firstName } ${ user.lastName }</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Testverwaltung</h3>
                        <ul class="nav side-menu">
                            <li><a href="/tests"><i class="fas fa-vial"></i> Tests</a></li>
                            <li><a href="/candidates"><i class="fas fa-users"></i> Bewerber</a></li>
                            <li><a href="/skills"><i class="fas fa-brain"></i> Skills</a></li>
                            <li><a href="/items"><i class="fas fa-tags"></i> Items</a></li>
                        </ul>
                    </div>
                    <#if user.type.isAdmin()>
                    <div class="menu_section">
                        <h3>Administration</h3>
                        <ul class="nav side-menu">
                            <li><a href="/admin/categories"><i class="fas fa-sitemap"></i> Kategorien</a></li>
                            <li><a><i class="fas fa-users"></i> Benutzer <i class="fas fa-chevron-down"></i></a>
                                <ul class="nav child_menu">
                                    <li><a href="/admin/customers">Unternehmen</a></li>
                                    <li><a href="/admin/admins">Administratoren</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    </#if>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="modal" data-target="#changePasswordModal" title="Benutzereinstellungen">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Abmelden" onclick="logout()">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a>${ user.firstName } ${ user.lastName }</a></li>
                        <#if session??>
                        <#if session.isSU()>
                        <li><a href="/admin/return"><i class="fas fa-reply"></i> Zurück zu Admin</a></li>
                        </#if>
                        </#if>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <@page_body />
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                <input type="text" id="clipboard-fake"><a target="_blank" href="${ branding.imprintUrl }">Impressum</a> | <a target="_blank" href="${ branding.privacyUrl }">Datenschutzerklärung</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="changePasswordModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="changePasswordModalLabel">Benutzereinstellungen</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Kennwort</label>
                    <input type="password" class="form-control" id="change-password-password">
                </div>
                <div class="form-group">
                    <label>Kennwort wiederholen</label>
                    <input type="password" class="form-control" id="change-password-repeat">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-primary" onclick="changePassword()">Speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="${cdn}/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="${cdn}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="${cdn}/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="${cdn}/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="${cdn}/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="${cdn}/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="${cdn}/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="${cdn}/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="${cdn}/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="${cdn}/vendors/Flot/jquery.flot.js"></script>
<script src="${cdn}/vendors/Flot/jquery.flot.pie.js"></script>
<script src="${cdn}/vendors/Flot/jquery.flot.time.js"></script>
<script src="${cdn}/vendors/Flot/jquery.flot.stack.js"></script>
<script src="${cdn}/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="${cdn}/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="${cdn}/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="${cdn}/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="${cdn}/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="${cdn}/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="${cdn}/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="${cdn}/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="${cdn}/vendors/moment/min/moment.min.js"></script>
<script src="${cdn}/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="${cdn}/build/js/custom.min.js"></script>

<script src="${cdn}/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${cdn}/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="${cdn}/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="${cdn}/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="${cdn}/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="${cdn}/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="${cdn}/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="${cdn}/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="${cdn}/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="${cdn}/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="${cdn}/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="${cdn}/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<script src="/js/jquery.nanoscroller.min.js"></script>

</body>
</html>