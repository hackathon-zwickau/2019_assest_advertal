<#macro page_body>
    <style>
        ul.wizard_steps {
            padding-inline-start: 0px;
        }
        ul.wizard_steps > li {
            width: 25%;
            display: table;
        }
        .wizard_horizontal ul.wizard_steps li:hover {
            display: table-cell;
            position: relative;
            color: #666;
        }
        .wizard-display {
            width: 100%;
        }
        .panel-body-skill {
            background-color: #34495E;
            color: white;
            padding-right: 0px;
            padding-bottom: 0px;
        }
        .panel-skill {
            -webkit-box-shadow: 6px 7px 12px -7px rgba(0,0,0,0.75);
            -moz-box-shadow: 6px 7px 12px -7px rgba(0,0,0,0.75);
            box-shadow: 6px 7px 12px -7px rgba(0,0,0,0.75);
            border: 0px solid black;
            margin-bottom: 5px;
        }
        .btn-skill-add {
            display: block;
            position: relative;
            width: 10%;
            margin: 0px;
            margin-left: 90%;
            border-radius: 0px;
        }
        .btn-set-category {
            cursor: pointer;
        }
    </style>
    <div class="x_panel">
        <div class="x_title">
            <h2>Assistent <small>Testerstellung</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a id="wizard-indicator-step-1" class="selected" rel="1">
                                            <span class="step_no">1</span>
                                            <span class="step_descr">
                                                Schritt 1<br>
                                                <small>Name, Beschreibung</small>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a id="wizard-indicator-step-2" class="disabled" rel="2">
                                            <span class="step_no">2</span>
                                            <span class="step_descr">
                                                Schritt 2<br>
                                                <small>Skills</small>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a id="wizard-indicator-step-3" class="disabled" rel="3">
                                            <span class="step_no">3</span>
                                            <span class="step_descr">
                                                Schritt 3<br>
                                                <small>Überprüfen</small>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <hr>
                            <div id="wizard-display-step-1" class="wizard-display">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" id="wizard-base-name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Beschreibung</label>
                                            <input type="text" id="wizard-base-description" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="wizard-display-step-2" class="wizard-display" style="display: none;">
                                <h3 class="text-center">Wählen Sie die Skills</h3>
                                <table style="border: 1px solid lightgray; width: 100%;">
                                    <tr style="border: 1px solid lightgray;">
                                        <td style="width: 25%; padding: 5px; border: 1px solid lightgray;">
                                            <h4>Kategorien</h4>
                                        </td>
                                        <td style="width: 50%; padding: 5px; border: 1px solid lightgray;">
                                            <h4>Verfügbare Skills</h4>
                                        </td>
                                        <td style="width: 25%; padding: 5px; border: 1px solid lightgray;">
                                            <h4>Ihr Test</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%; padding: 5px; border: 1px solid lightgray;" valign="top">
                                            <span class="btn-set-category" onclick="wizard.selection.setCategory(-1)">Alle</span><br>
                                            ${ displayCategories }
                                            <span class="btn-set-category" onclick="wizard.selection.setCategory(0)">Nicht zugewiesen</span>
                                        </td>
                                        <td style="width: 25%; height: 100%; padding: 5px; border: 1px solid lightgray;">
                                            <div style="height: 100%; min-height: 400px;" class="nano has-scrollbar">
                                                <div class="nano-content">
                                                    <div id="wizard-area-skills-available" style="width: 95%;">

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 25%; padding: 5px; border: 1px solid lightgray;" id="wizard-area-skills-selected" valign="top"></td>
                                    </tr>
                                </table>
                            </div>
                            <div id="wizard-display-step-3" class="wizard-display" style="display: none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3>Überblick</h3>
                                        <b>Name</b><br><span id="wizard-validation-name"></span><br><br>
                                        <b>Beschreibung</b><br><span id="wizard-validation-description"></span><br><br>
                                        <b>Anzahl der Fragen</b><br><span id="wizard-validation-items"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <h3>Ausgewählte Skills</h3>
                                        <div id="wizard-validation-skills"></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="text-center">
                                <button onclick="wizard.prev()" class="btn btn-primary" id="wizard-prev-button" style="display: none;">Zurück</button>
                                <button onclick="wizard.next()" class="btn btn-primary">Weiter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/wizard.js"></script>
</#macro>
<#include "base.ftl" />