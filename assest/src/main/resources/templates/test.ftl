<#macro page_body>
    <div class="x_panel">
        <div class="x_title" style="padding-bottom: 0px;">
            <h2>Test <small>${ test.name }</small></h2>
            <div class="" role="tabpanel" data-example-id="togglable-tabs" style="postion: absolute; bottom: 0px;">
                <ul class="nav nav-tabs bar_tabs right" role="tablist">
                    <li role="presentation"><a href="/wordings/${ test.id }" role="tab">Auswertungstexte</a></li>
                    <li role="presentation"><a href="/auswertung/test/${ test.id }" role="tab">Gesamtauswertung</a></li>
                </ul>
            </div>
        </div>
        <div class="x_content">
            ${ test.displayDescription }
        </div>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>Durchläufe</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a><button class="btn btn-primary" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#createRunModal">Durchlauf erstellen</button></a></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="testRunsTable" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                <tr>
                    <th>Benutzer</th>
                    <th>Status</th>
                    <th>Schlüssel</th>
                    <th>Aktionen</th>
                    <th>Erstellt am</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="createRunModal" tabindex="-1" role="dialog" aria-labelledby="createRunModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createRunModalLabel">Durchlauf erstellen</h4>
                </div>
                <div class="modal-body">
                    <#if user.getType().isAdmin() && test.getCustomerId() == 0>
                    <div class="form-group" id="create-run-area-customer">
                        <label>Kunde auswählen</label>
                        <select class="form-control" id="create-run-customer" onchange="getSubUsers();">
                            <#list customers as customer>
                                <option value="${ customer.id }">${ customer.firstName } ${ customer.lastName }</option>
                            </#list>
                        </select>
                    </div>
                    <div class="form-group" id="create-run-area-user">
                        <label>Benutzer auswählen</label>
                        <select class="form-control" id="create-run-user">
                            <#list subusers as subuser>
                            <option value="${ subuser.id }">${ subuser.firstName } ${ subuser.lastName }</option>
                            </#list>
                        </select>
                    </div>
                    <#else>
                    <div class="form-group">
                        <label>Benutzer auswählen</label>
                        <select class="form-control" id="create-run-user">
                            <#list subusers as subuser>
                            <option value="${ subuser.id }">${ subuser.firstName } ${ subuser.lastName }</option>
                            </#list>
                        </select>
                    </div>
                    </#if>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" onclick="createRun()">Erstellen</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        var createRunStep = <#if user.getType().isAdmin() && test.getCustomerId() == 0>1<#else>0</#if>;
        function createRun(){
            if(createRunStep == 0){
                var user_id = $('#create-run-user').val();
                let body = {
                    test_id: ${ test.id },
                    user_id: user_id
                };
                $.ajax({
                    type: "POST",
                    url: "/api/runs",
                    data: JSON.stringify(body),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).done(function( data ) {
                    $('#createRunModal').modal('hide');
                    alertSuccess(data,function(){
                        if(data.success){
                            testRunsTable.ajax.reload();
                        }
                    });
                });
            }
            if(createRunStep == 1){
                var customer_id = $('#create-run-customer').val();
                var user_id = $('#create-run-user').val();
                let body = {
                    test_id: ${ test.id },
                    customer_id: customer_id,
                    user_id: user_id
                };
                $.ajax({
                    type: "POST",
                    url: "/api/runs",
                    data: JSON.stringify(body),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).done(function( data ) {
                    $('#createRunModal').modal('hide');
                    alertSuccess(data,function(){
                        if(data.success){
                            testRunsTable.ajax.reload();
                        }
                    });
                });
            }
        }
        function recalcRun(id){
            $.ajax({
                type: "POST",
                url: "/api/runs/"+id+"/recalc",
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        testRunsTable.ajax.reload();
                    }
                });
            });
        }
        function sendRunMail(id){
            $.ajax({
                type: "POST",
                url: "/api/runs/"+id+"/mail",
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        testRunsTable.ajax.reload();
                    }
                });
            });
        }
        function deleteRun(id){
            $.ajax({
                type: "DELETE",
                url: "/api/runs/"+id,
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        testRunsTable.ajax.reload();
                    }
                });
            });
        }
        <#if user.getType().isAdmin()>
        function getSubUsers(event){
            console.log("getSubUsers");
            var customer_id = $('#create-run-customer').val();
            $.ajax({
                type: "GET",
                url: "/api/subusers/"+customer_id,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function( data ) {
                var select = document.getElementById("create-run-user");
                while (select.firstChild) {
                    select.removeChild(select.firstChild);
                }
                data.forEach(user => {
                    var option = document.createElement("option");
                    option.setAttribute("value", user.id);
                    option.innerHTML = user.firstName+" "+user.lastName;
                    select.appendChild(option);
                });
            });
        }
        </#if>
    </script>
    <script>
        var testRunsTable;
        $(document).ready( function () {
            testRunsTable = $('#testRunsTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
                },
                ajax: "/table/tests/test/${ test.id }/runs"
            });
        });
    </script>
</#macro>
<#include "base.ftl" />