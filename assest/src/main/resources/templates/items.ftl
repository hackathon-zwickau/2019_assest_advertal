<#macro page_body>
    <div class="x_panel">
        <div class="x_title">
            <h2>Inhalte <small>Items</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a><button class="btn btn-primary" data-toggle="modal" data-target="#createItemModal">Neues Item</button></a></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="itemsTable" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                    <tr>
                        <th>Frage</th>
                        <th>Skill</th>
                        <th>Typ</th>
                        <th>Aktionen</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="createItemModal" tabindex="-1" role="dialog" aria-labelledby="createItemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createItemModalLabel">Item erstellen</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Skill</label>
                        <select class="form-control" id="create-item-skill">
                            <#list skills as skill>
                            <option value="${ skill.id }">${ skill.name }</option>
                            </#list>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Frage</label>
                        <input class="form-control" id="create-item-question">
                    </div>
                    <div class="form-group">
                        <label>Medien</label>
                        <select class="form-control" id="create-item-media-type" onchange="onMediaChange('create')">
                            <option value="NONE">Keine</option>
                            <option value="IMAGE">Bild</option>
                        </select>
                    </div>
                    <div class="form-group" id="create-item-area-media-url" style="display: none;">
                        <label>Medien URL</label>
                        <input class="form-control" id="create-item-media-url">
                    </div>
                    <div class="form-group">
                        <label>Typ</label>
                        <select class="form-control" id="create-item-type" onchange="onTypeChange('create')">
                            <option value="LIKERT4">Likert 4</option>
                            <option value="LIKERT5">Likert 5</option>
                            <option value="SINGLE_CHOICE">Single Choice</option>
                            <option value="MULTIPLE_CHOICE">Multiple Choice</option>
                            <option value="PYTHON">Python Codestyle</option>
                        </select>
                    </div>
                    <div id="create-item-area-answers" style="display: none;">
                        <div class="form-group">
                            <label>Antworten</label>
                            <textarea id="create-item-answers" class="form-control" rows="5" data-limit-rows="true"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Richtige Antwort(en)</label>
                            <textarea id="create-item-correct" class="form-control" rows="1" data-limit-rows="true"></textarea>
                        </div>
                    </div>
                    <div class="form-group" id="create-item-area-invert">
                        <label>Auswertung</label>
                        <select class="form-control" id="create-item-invert">
                            <option value="0">Normal</option>
                            <option value="1">Invertiert</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" onclick="createItem()">Erstellen</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateItemModal" tabindex="-1" role="dialog" aria-labelledby="updateItemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="updateItemModalLabel">Item bearbeiten</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="update-item-id" value="0">
                    <div class="form-group">
                        <label>Frage</label>
                        <input class="form-control" id="update-item-question">
                    </div>
                    <div class="form-group">
                        <label>Medien</label>
                        <select class="form-control" id="update-item-media-type" onchange="onMediaChange('update')">
                            <option value="NONE">Keine</option>
                            <option value="IMAGE">Bild</option>
                        </select>
                    </div>
                    <div class="form-group" id="update-item-area-media-url" style="display: none;">
                        <label>Medien URL</label>
                        <input class="form-control" id="update-item-media-url">
                    </div>
                    <div class="form-group">
                        <label>Typ</label>
                        <select class="form-control" id="update-item-type" onchange="onTypeChange('update')">
                            <option value="LIKERT4">Likert 4</option>
                            <option value="LIKERT5">Likert 5</option>
                            <option value="SINGLE_CHOICE">Single Choice</option>
                            <option value="MULTIPLE_CHOICE">Multiple Choice</option>
                            <option value="PYTHON">Python Codestil</option>
                        </select>
                    </div>
                    <div id="update-item-area-answers" style="display: none;">
                        <div class="form-group">
                            <label>Antworten</label>
                            <textarea id="update-item-answers" class="form-control" rows="5" data-limit-rows="true"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Richtige Antwort(en)</label>
                            <textarea id="update-item-correct" class="form-control" rows="1" data-limit-rows="true"></textarea>
                        </div>
                    </div>
                    <div class="form-group" id="update-item-area-invert">
                        <label>Auswertung</label>
                        <select class="form-control" id="update-item-invert">
                            <option value="0">Normal</option>
                            <option value="1">Invertiert</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" onclick="updateItem()">Speichern</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function onTypeChange(area){
            var type = $("#"+area+"-item-type").val();
            if(type == "SINGLE_CHOICE"){
                $("#"+area+"-item-correct").attr("rows", 1);
                $("#"+area+"-item-area-invert").hide();
                $("#"+area+"-item-area-answers").show();
            }else if(type == "MULTIPLE_CHOICE"){
                $("#"+area+"-item-area-answers").show();
                $("#"+area+"-item-area-invert").hide();
                $("#"+area+"-item-correct").attr("rows", 5);
            }else if(type == "PYTHON"){
                $("#"+area+"-item-correct").attr("rows", 1);
                $("#"+area+"-item-area-invert").hide();
                $("#"+area+"-item-area-answers").show();
            }else{
                $("#"+area+"-item-area-answers").hide();
                $("#"+area+"-item-area-invert").show();
            }
        }
        function onMediaChange(area){
            var type = $("#"+area+"-item-media-type").val();
            if(type == "NONE"){
                $("#"+area+"-item-area-media-url").hide();
            }else{
                $("#"+area+"-item-area-media-url").show();
            }
        }
        function deleteItem(id){
            $.ajax({
                type: "DELETE",
                url: "/api/items/"+id,
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        itemsTable.ajax.reload();
                    }
                });
            });
        }
        function createItem(){
            var correct = [];
            var answers = $("#create-item-answers").val().split("\n").filter(value => value!="");
            var correctStrings = $("#create-item-correct").val().split("\n").filter(value => value!="");
            correctStrings.forEach(s => {
                for(var i=0;i<answers.length;i++){
                    if(answers[i] == s){
                        correct.push(i+1);
                        break;
                    }
                }
            });
            $('#create-item-answers').val("");
            $('#create-item-correct').val("");
            var question = $('#create-item-question').val();
            $('#create-item-question').val("");
            var type = $('#create-item-type').val();
            var skill = $('#create-item-skill').val();
            var invert = $('#create-item-invert').val();
            if(type == "SINGLE_CHOICE"){
                if(correct.length > 0){
                    correct = [correct[0]];
                }
            }
            if(invert == "1" || invert == 1){
                invert = true;
            }else{
                invert = false;
            }
            let body = {
                question: question,
                type: type,
                skill: skill,
                invert: invert,
                answers: JSON.stringify(answers),
                correct: correct.join(","),
                mediaType: $('#create-item-media-type').val(),
                media: $('#create-item-media-url').val()
            };
            $('#create-item-media-url').val("");
            $.ajax({
                type: "POST",
                url: "/api/items",
                data: JSON.stringify(body),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function( data ) {
                $('#createItemModal').modal('hide');
                alertSuccess(data,function(){
                    if(data.success){
                        itemsTable.ajax.reload();
                    }
                });
            });
        }
        function updateItem(){
            var correct = [];
            var answers = $("#update-item-answers").val().split("\n").filter(value => value!="");
            var correctStrings = $("#update-item-correct").val().split("\n").filter(value => value!="");
            correctStrings.forEach(s => {
                for(var i=0;i<answers.length;i++){
                    if(answers[i] == s){
                        correct.push(i+1);
                        break;
                    }
                }
            });
            var id = $('#update-item-id').val();
            var question = $('#update-item-question').val();
            var invert = $('#update-item-invert').val();
            var type = $('#update-item-type').val();
            if(invert == "1" || invert == 1){
                invert = true;
            }else{
                invert = false;
            }
            if(type == "SINGLE_CHOICE"){
                if(correct.length > 0){
                    correct = [correct[0]];
                }
            }
            let body = {
                question: question,
                invert: invert,
                type: type,
                answers: JSON.stringify(answers),
                correct: correct.join(","),
                mediaType: $('#update-item-media-type').val(),
                media: $('#update-item-media-url').val()
            };
            $.ajax({
                type: "POST",
                url: "/api/items/"+id,
                data: JSON.stringify(body),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function( data ) {
                $('#updateItemModal').modal('hide');
                alertSuccess(data,function(){
                    if(data.success){
                        itemsTable.ajax.reload();
                    }
                });
            });
        }
        function editItem(id){
            $.ajax({
                type: "GET",
                url: "/api/items/"+id,
                dataType: "json"
            }).done(function( data ) {
                if(data.success){
                    $("#update-item-id").val(id);
                    $("#update-item-type").val(data.type);
                    $("#update-item-question").val(data.question);
                    $("#update-item-answers").val(data.answers.join("\n"));
                    $("#update-item-correct").val(data.correct.join("\n"));
                    $("#update-item-media-type").val(data.mediaType);
                    $("#update-item-media-url").val(data.media);
                    $("#update-item-invert").val(data.invert?"1":"0");
                    onTypeChange("update");
                    onMediaChange("update");
                    $("#updateItemModal").modal('show');
                }
            });
        }
        var itemsTable;
        $(document).ready( function () {
            itemsTable = $('#itemsTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
                },
                ajax: "/table/items/all"
            });
        });
        $(document).ready(function () {
            $('textarea[data-limit-rows=true]').on('keypress', function (event) {
                var textarea = $(this),
                text = textarea.val(),
                numberOfLines = (text.match(/\n/g) || []).length + 1,
                maxRows = parseInt(textarea.attr('rows'));
                if (event.which === 13 && numberOfLines === maxRows ) {
                  return false;
                }
            });
        });
    </script>
</#macro>
<#include "base.ftl" />