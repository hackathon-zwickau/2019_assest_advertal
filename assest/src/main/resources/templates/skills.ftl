<#macro page_body>
    <div class="x_panel">
        <div class="x_title">
            <h2>Inhalte <small>Skills</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a><button class="btn btn-primary" data-toggle="modal" data-target="#createSkillModal">Neuer Skill</button></a></li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="skillsTable" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Kategorie</th>
                        <th>Aktionen</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="createSkillModal" tabindex="-1" role="dialog" aria-labelledby="createSkillModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createSkillModalLabel">Skill erstellen</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" id="create-skill-name">
                    </div>
                    <div class="form-group">
                        <label>Beschreibung</label>
                        <input class="form-control" id="create-skill-description">
                    </div>
                    <div class="form-group">
                        <label>Typ</label>
                        <select class="form-control" id="create-skill-type">
                            <option value="1">Softskill</option>
                            <option value="0">Fachwissen</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kategorie</label>
                        <select class="form-control" id="create-skill-category">
                            <option value="0">Keine</option>
                            <#list categories as category>
                            <option value="${ category.id }">${ category.name }</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" onclick="createSkill()">Erstellen</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateSkillModal" tabindex="-1" role="dialog" aria-labelledby="updateSkillModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="updateSkillModalLabel">Skill bearbeiten</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="update-skill-id" value="0">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" id="update-skill-name">
                    </div>
                    <div class="form-group">
                        <label>Beschreibung</label>
                        <input class="form-control" id="update-skill-description">
                    </div>
                    <div class="form-group">
                        <label>Typ</label>
                        <select class="form-control" id="update-skill-type">
                            <option value="1">Softskill</option>
                            <option value="0">Fachwissen</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kategorie</label>
                        <select class="form-control" id="update-skill-category">
                            <option value="0">Keine</option>
                            <#list categories as category>
                            <option value="${ category.id }">${ category.name }</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" onclick="updateSkill()">Speichern</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function deleteSkill(id){
            $.ajax({
                type: "DELETE",
                url: "/api/skills/"+id,
                dataType: "json"
            }).done(function( data ) {
                alertSuccess(data,function(){
                    if(data.success){
                        window.location.reload();
                    }
                });
            });
        }
        function createSkill(){
            var name = $('#create-skill-name').val();
            var description = $('#create-skill-description').val();
            var soft = $('#create-skill-type').val();
            if(soft == 1 || soft == "1"){
                soft = true;
            }else{
                soft = false;
            }
            var categoryId = $('#create-skill-category').val();
            let body = {
                name: name,
                description: description,
                soft: soft,
                categoryId: categoryId
            };
            $.ajax({
                type: "POST",
                url: "/api/skills",
                data: JSON.stringify(body),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function( data ) {
                $('#createSkillModal').modal('hide');
                alertSuccess(data,function(){
                    if(data.success){
                        window.location.reload();
                    }
                });
            });
        }
        function updateSkill(){
            var id = $('#update-skill-id').val();
            var name = $('#update-skill-name').val();
            var description = $('#update-skill-description').val();
            var soft = $('#update-skill-type').val();
            var category = $('#update-skill-category').val();
            if(soft == "1" || soft == 1){
                soft = true;
            }else{
                soft = false;
            }
            let body = {
                name: name,
                description: description,
                soft: soft,
                category: category
            };
            $.ajax({
                type: "POST",
                url: "/api/skills/"+id,
                data: JSON.stringify(body),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function( data ) {
                $('#updateSkillModal').modal('hide');
                alertSuccess(data,function(){
                    if(data.success){
                        skillsTable.ajax.reload();
                    }
                });
            });
        }
        function editSkill(id){
            $.ajax({
                type: "GET",
                url: "/api/skills/"+id,
                dataType: "json"
            }).done(function( data ) {
                if(data.success){
                    $("#update-skill-id").val(id);
                    $("#update-skill-name").val(data.name);
                    $("#update-skill-description").val(data.description);
                    $("#update-skill-type").val(data.soft?"1":"0");
                    $("#update-skill-category").val(data.category);
                    $("#updateSkillModal").modal('show');
                }
            });
        }
        var skillsTable;
        $(document).ready( function () {
            skillsTable = $('#skillsTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
                },
                ajax: "/table/skills/all"
            });
        });
    </script>
</#macro>
<#include "base.ftl" />