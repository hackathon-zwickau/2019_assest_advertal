<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${ branding.name }</title>

    <link href="${cdn}/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="${cdn}/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="${cdn}/vendors/nprogress/nprogress.css" rel="stylesheet">

    <link href="${cdn}/vendors/animate.css/animate.min.css" rel="stylesheet">

    <link href="${cdn}/build/css/custom.min.css" rel="stylesheet">
</head>
<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form id="login-form">
                    <h1>Anmelden</h1>
                    <div>
                        <input id="login-email" type="email" class="form-control" placeholder="E-Mail" required="" />
                    </div>
                    <div>
                        <input id="login-password" type="password" class="form-control" placeholder="Kennwort" required="" />
                    </div>
                    <div>
                        <button type="submit" class="btn btn-default btn-block">Anmelden</button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">
                        <h1>${ branding.name }</h1>
                        <p>©2019 ${ branding.name }</p>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="/js/helper.js"></script>
<script src="/js/login.js"></script>
</body>
</html>