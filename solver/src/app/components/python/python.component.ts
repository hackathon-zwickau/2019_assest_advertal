import { Component, OnInit } from '@angular/core';
import {CodeStyleBaseComponent} from '../../models/CodeStyleBaseComponent';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-python',
  templateUrl: './python.component.html',
  styleUrls: ['./python.component.scss']
})
export class PythonComponent implements OnInit {

  constructor(private parent: AppComponent) {
  }

  code = "";

  next() {
    console.log(this.code);
    this.parent.submit(this.code);
  }

  ngOnInit() {
  }

}
