import { Component, OnInit } from '@angular/core';
import {SingleChoiceBaseComponent} from '../../models/SingleChoiceBaseComponent';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-singlechoice',
  templateUrl: './singlechoice.component.html',
  styleUrls: ['./singlechoice.component.scss']
})
export class SingleChoiceComponent extends SingleChoiceBaseComponent implements OnInit {

  constructor(parent: AppComponent) {
    super(parent);
  }

  ngOnInit() {
  }

}
