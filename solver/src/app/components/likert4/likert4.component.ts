import { Component, OnInit } from '@angular/core';
import {SingleChoiceBaseComponent} from '../../models/SingleChoiceBaseComponent';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-likert4',
  templateUrl: './likert4.component.html',
  styleUrls: ['./likert4.component.scss']
})
export class Likert4Component extends SingleChoiceBaseComponent implements OnInit {

  constructor(parent: AppComponent) {
    super(parent);
  }

  ngOnInit() {
  }

}
