import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Likert4Component } from './likert4.component';

describe('Likert4Component', () => {
  let component: Likert4Component;
  let fixture: ComponentFixture<Likert4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Likert4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Likert4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
