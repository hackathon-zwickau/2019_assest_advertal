import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Likert5Component } from './likert5.component';

describe('Likert5Component', () => {
  let component: Likert5Component;
  let fixture: ComponentFixture<Likert5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Likert5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Likert5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
