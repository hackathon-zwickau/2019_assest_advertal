import { Component, OnInit } from '@angular/core';
import {SingleChoiceBaseComponent} from '../../models/SingleChoiceBaseComponent';
import {AppComponent} from '../../app.component';

// @ts-ignore
@Component({
  selector: 'app-likert5',
  templateUrl: './likert5.component.html',
  styleUrls: ['./likert5.component.scss']
})
export class Likert5Component extends SingleChoiceBaseComponent implements OnInit {

  constructor(parent: AppComponent) {
    super(parent);
  }

  ngOnInit() {
  }

}
