import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../../app.component';
import {MultipleChoiceBaseComponent} from '../../models/MultipleChoiceBaseComponent';

@Component({
  selector: 'app-multiplechoice',
  templateUrl: './multiplechoice.component.html',
  styleUrls: ['./multiplechoice.component.scss']
})
export class MultipleChoiceComponent extends MultipleChoiceBaseComponent implements OnInit {

  constructor(parent: AppComponent) {
    super(parent);
  }

  ngOnInit() {
  }

}
