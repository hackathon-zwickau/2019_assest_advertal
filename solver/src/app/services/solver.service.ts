import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ItemResponse} from '../models/api/ItemResponse';

@Injectable({
  providedIn: 'root'
})
export class SolverService {

  api: string;

  constructor(private http: HttpClient) {
    this.api = 'http://localhost:8080';
  }

  getItem(token: string): Observable<ItemResponse> {
    return this.http.get<ItemResponse>(this.api + '/api/solve/run/' + token);
  }

  postItem(token: string, answer: string): Observable<ItemResponse> {
    return this.http.post<ItemResponse>(this.api + '/api/solve/run/' + token, answer, {
      headers: new HttpHeaders({
        'Content-Type':  'text/plain'
      })
    });
  }

}
