export class Item {
  type: ItemType;
  question: string;
  answers: string[];
  mediaType: MediaType;
  media: string;
}

export enum ItemType {
  NONE = 'NONE',
  LIKERT4 = 'LIKERT4',
  LIKERT5 = 'LIKERT5',
  SINGLE_CHOICE = 'SINGLE_CHOICE',
  MULTIPLE_CHOICE = 'MULTIPLE_CHOICE',
  PYTHON = 'PYTHON'
}

export enum MediaType {
  NONE = 'NONE',
  IMAGE = 'IMAGE'
}
