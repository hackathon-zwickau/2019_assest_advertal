import {Item} from './Item';

export class ItemResponse {
  success: boolean;
  test: string;
  progress: number;
  data: Item;
}
