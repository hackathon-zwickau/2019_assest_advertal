import {AppComponent} from '../app.component';
import {ItemBaseComponent} from './ItemBaseComponent';

export class MultipleChoiceBaseComponent extends ItemBaseComponent {
  btn: number[] = [];
  showNext = false;
  constructor(public parent: AppComponent) {
    super();
  }
  getButtonClass(btn: number) {
    return this.btn.includes(btn) ? 'primary' : 'default';
  }
  selectButton(btn: number) {
    if (this.btn.includes(btn)) {
      this.btn = this.btn.filter(value => btn !== value);
    } else {
      this.btn.push(btn);
    }
    this.showNext = true;
  }
  next() {
    this.parent.submit(this.btn.join(','));
    this.showNext = false;
  }
}
