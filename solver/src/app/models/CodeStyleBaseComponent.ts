import {AppComponent} from '../app.component';
import {ItemBaseComponent} from './ItemBaseComponent';

export class CodeStyleBaseComponent extends ItemBaseComponent {
  code = "";
  constructor(public parent: AppComponent) {
    super();
  }
  next() {
    console.log(this.code);
    this.parent.submit(this.code);
  }
}
