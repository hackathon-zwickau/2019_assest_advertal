import {AppComponent} from '../app.component';
import {ItemBaseComponent} from './ItemBaseComponent';

export class SingleChoiceBaseComponent extends ItemBaseComponent {
  btn: number;
  showNext = false;
  constructor(public parent: AppComponent) {
    super();
  }
  getButtonClass(btn: number) {
    return btn === this.btn ? 'primary' : 'default';
  }
  selectButton(btn: number) {
    this.btn = btn;
    this.showNext = true;
  }
  next() {
    this.parent.submit(this.btn);
  }
}
