import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Likert4Component } from './components/likert4/likert4.component';
import { Likert5Component } from './components/likert5/likert5.component';
import {HttpClientModule} from '@angular/common/http';
import {SolverService} from './services/solver.service';
import {FormsModule} from '@angular/forms';
import { SingleChoiceComponent } from './components/singlechoice/singlechoice.component';
import { MultipleChoiceComponent } from './components/multiplechoice/multiplechoice.component';
import { PythonComponent } from './components/python/python.component';

@NgModule({
  declarations: [
    AppComponent,
    Likert4Component,
    Likert5Component,
    SingleChoiceComponent,
    MultipleChoiceComponent,
    PythonComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    SolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
