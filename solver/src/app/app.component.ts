import {Component} from '@angular/core';
import {SolverState} from './models/SolverState';
import {Item, ItemType} from './models/api/Item';
import {SolverService} from './services/solver.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  SolverState: any = SolverState;
  ItemType: any = ItemType;
  state: SolverState = SolverState.START;
  test = '';
  progress = 0;
  item: Item = new Item();
  token = '';
  constructor(private solver: SolverService) {
    let hash = window.location.hash;
    if (hash.length > 0) {
      hash = hash.substring(1, hash.length);
      this.token = hash;
      this.start();
    }
  }
  start() {
    this.solver.getItem(this.token).subscribe(response => {
      if (response.success) {
        this.item = response.data;
        this.test = response.test;
        this.progress = response.progress;
        this.state = SolverState.SOLVE;
      } else {
        if (response.progress === 100) {
          this.test = response.test;
          this.progress = response.progress;
          this.state = SolverState.DONE;
        }
      }
    });
  }
  submit(value: any) {
    console.log(value);
    this.solver.postItem(this.token, value).subscribe(response => {
      console.log(response.success);
      if (response.success) {
        this.test = response.test;
        this.progress = response.progress;
        if (this.progress < 100) {
          this.item = response.data;
        } else {
          this.state = SolverState.DONE;
        }
      }
    });
  }
}
